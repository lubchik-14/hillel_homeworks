package com.hillel.archive.hw3.HW3.src;

public class Vehicle {
    int id;
    String brand;
    String model;
    int modelYear;
    String color;
    double price;
    String licensePlate;

    Vehicle (int id, String brand, String model, int modelYear, double price, String licensePlate) {
        this.id = id;
        this.brand = brand;
        this.model = model;
        this.modelYear = modelYear;
        this.price = price;
        this.licensePlate = licensePlate;
    }
}
