package com.hillel.archive.hw3.HW3.src;

import java.time.Year;
import java.util.Arrays;

public class VehicleProcessor {
    Vehicle[] vehicles;

    VehicleProcessor (Vehicle[] vehicles) {
        this.vehicles = vehicles;
    }

    void printVehicle(Vehicle[] vehicles) {
        for (Vehicle i : vehicles) {
            System.out.println(i.id + ": " + i.brand + " " + i.model + " " + i.modelYear + " " +
                    i.color + " " + "$" + i.price + " " + i.licensePlate);
        }
    }

    Vehicle[] findByBrand(String brand) {
        Vehicle[] filtered = new Vehicle[0];
        for (Vehicle i : vehicles) {
            if (i.brand.equals(brand)) {
                filtered = Arrays.copyOf(filtered, filtered.length + 1);
                filtered[filtered.length - 1] = i;
            }
        }
        return filtered;
    }

    Vehicle[] findByModelAndModelYearGraterThan(String model, int years){
        Vehicle[] filtered = new Vehicle[0];
        int currentYear = Year.now().getValue();
        for (Vehicle i : vehicles) {
            if ((i.model.equals(model)) && ((currentYear - i.modelYear) > years)) {
                filtered = Arrays.copyOf(filtered, filtered.length + 1);
                filtered[filtered.length - 1] = i;
            }
        }
        return filtered;
    }

    Vehicle[] findByModelYearAndPriceGrateThan(int modelYear, double price) {
        Vehicle[] filtered = new Vehicle[0];
        for (Vehicle i : vehicles) {
            if ((i.modelYear == modelYear) && (i.price > price)) {
                filtered = Arrays.copyOf(filtered, filtered.length + 1);
                filtered[filtered.length - 1] = i;
            }
        }
        return filtered;
    }
}
