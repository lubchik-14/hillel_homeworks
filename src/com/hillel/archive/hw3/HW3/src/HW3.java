package com.hillel.archive.hw3.HW3.src;

public class HW3 {
    public static void main (String args[]) {
        Vehicle[] arrayOfVehicles = {
                new Vehicle(1, "BMW", "X5", 2019, 555555, "ЕР6666УК"),
                new Vehicle(2, "BMW", "A3", 2010, 140000, "ЗЕ6666ПР"),
                new Vehicle(3, "BMW", "330", 2000, 44999.99, "ЛЯ6666ЛЯ"),
                new Vehicle(4, "Chevrolet", "Impala", 1958, 100500, "ГВ6666КД"),
                new Vehicle(5, "Chevrolet", "Lacetti", 1990, 79797, "ЧО6666ХО"),
                new Vehicle(6, "Chevrolet", "Aveo", 1984, 999999.99, "ОЙ6666ОЙ"),
                new Vehicle(7, "Audi", "R8", 2019, 66666, "СИ6666ГА"),
                new Vehicle(8, "Audi", "A3", 2018, 29000, "ЛЮ6666БА"),
                new Vehicle(8, "Audi", "A3", 2000, 696969, "ХХ6666ХХ"),
                new Vehicle(9, "Mercedes", "S600", 1950, 100000, "ХВ6666ВВ"),
                new Vehicle(10, "ZAZ", "966", 1967, 10000, "АХ9999ХЗ"),
        };
        VehicleProcessor vehicleProcessor = new VehicleProcessor(arrayOfVehicles);
        System.out.println("Audi full list");
        vehicleProcessor.printVehicle(vehicleProcessor.findByBrand("Audi"));
        System.out.println("Used A3");
        vehicleProcessor.printVehicle(vehicleProcessor.findByModelAndModelYearGraterThan("A3", 5));
        System.out.println("New luxury");
        vehicleProcessor.printVehicle(vehicleProcessor.findByModelYearAndPriceGrateThan(2019, 100000));
    }
}
