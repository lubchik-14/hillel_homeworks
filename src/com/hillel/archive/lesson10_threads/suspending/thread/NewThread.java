package com.hillel.archive.lesson10_threads.suspending.thread;

public class NewThread implements Runnable{
    private String name;
    private boolean suspend;
    private Thread t;

    public NewThread(String name) {
        this.name = name;
        suspend = false;
        t = new Thread(this, name);
        System.out.println("New thread " + name);
        t.start();
    }

    @Override
    public void run() {
        try {
            for (int i = 15; i > 0; i--) {
                System.out.println(name + " : " + i);
                Thread.sleep(200);
                synchronized (this) {
                    while (suspend) {
                        wait();
                    }
                }
            }
        } catch (InterruptedException e) {
            System.out.println(name + " has just been interrupted");
        }
        System.out.println(name + " is exiting");
    }

    public synchronized void mySuspend () {
        suspend = true;
    }

    public synchronized void myResume () {
        suspend = false;
        notify();
    }

    public String getName() {
        return name;
    }

    public Thread getT() {
        return t;
    }
}
