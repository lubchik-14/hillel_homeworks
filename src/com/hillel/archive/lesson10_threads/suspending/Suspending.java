package com.hillel.archive.lesson10_threads.suspending;

import com.hillel.archive.lesson10_threads.suspending.thread.NewThread;

public class Suspending {
    public static void main(String[] args) {
        NewThread thread_1 = new NewThread("thread_1");
        NewThread thread_2 = new NewThread("thread_2");

        try {
            Thread.sleep(1000);
            System.out.println("Suspending " + thread_1.getName());
            thread_1.mySuspend();
            Thread.sleep(1000);
            System.out.println("Resuming " + thread_1.getName());
            thread_1.myResume();
            Thread.sleep(1000);
            System.out.println("Suspending " + thread_2.getName());
            thread_2.mySuspend();
            Thread.sleep(1000);
            System.out.println("Resuming " + thread_2.getName());
            thread_2.myResume();
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            System.out.println("Main thread has just been interrupted");
        }

        try {
            System.out.println("Waiting threads to finish");
            thread_1.getT().join();
            thread_2.getT().join();
        } catch (InterruptedException e) {
            System.out.println("Main thread has just been interrupted");
        }
        System.out.println("Main thread is exiting");
    }
}
