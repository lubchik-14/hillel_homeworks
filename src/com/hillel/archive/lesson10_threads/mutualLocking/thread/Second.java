package com.hillel.archive.lesson10_threads.mutualLocking.thread;

public class Second {
    public synchronized void doSmth(First first) {
        String name = Thread.currentThread().getName();

        System.out.println(name + " entered to second.doSmth");

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            System.out.println("InterruptedException caught at first");
        }

        System.out.println(name + "is trying to call first.doOtherSmth");
        first.doOtherSmth();
    }

    public synchronized void doOtherSmth() {
        System.out.println("Inside second.doOtherSmth");
    }
}
