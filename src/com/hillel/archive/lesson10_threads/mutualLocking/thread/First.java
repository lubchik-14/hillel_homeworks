package com.hillel.archive.lesson10_threads.mutualLocking.thread;

public class First {
    public synchronized void doSmth(Second second) {
        String name = Thread.currentThread().getName();

        System.out.println(name + " entered to first.doSmth");

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            System.out.println("InterruptedException caught at first");
        }

        System.out.println(name + "is trying to call second.doOtherSmth");
        second.doOtherSmth();
    }

    public synchronized void doOtherSmth() {
        System.out.println("Inside first.doOtherSmth");
    }
}
