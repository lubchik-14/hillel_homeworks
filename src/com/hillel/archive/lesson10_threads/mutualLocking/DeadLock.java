package com.hillel.archive.lesson10_threads.mutualLocking;

import com.hillel.archive.lesson10_threads.mutualLocking.thread.First;
import com.hillel.archive.lesson10_threads.mutualLocking.thread.Second;

public class DeadLock implements Runnable {
    First first = new First();
    Second second = new Second();

    public DeadLock() {
        Thread.currentThread().setName("MainThread");
        Thread t = new Thread(this, "SecondThread");
        t.start();

        first.doSmth(second);
        System.out.println("Back in main thread");
    }

    @Override
    public void run() {
        second.doSmth(first);
        System.out.println("Back in second thread");
    }

    public static void main(String[] args) {
        new DeadLock();
    }
}
