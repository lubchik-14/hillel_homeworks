package com.hillel.archive.lesson10_threads.waitingMonitor.thread;

import com.hillel.archive.lesson10_threads.waitingMonitor.data.Storage;

public class Producer implements Runnable {
    Storage storage;

    public Producer(Storage storage) {
        this.storage = storage;
        new Thread(this, "Putting").start();
    }

    @Override
    public void run() {
        int i = 0;
        while (true) {
            storage.put(i++);
        }
    }
}
