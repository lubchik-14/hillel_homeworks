package com.hillel.archive.lesson10_threads.waitingMonitor.thread;

import com.hillel.archive.lesson10_threads.waitingMonitor.data.Storage;

public class Customer implements Runnable {
    Storage storage;

    public Customer(Storage storage) {
        this.storage = storage;
        new Thread(this, "Getting").start();
    }

    @Override
    public void run() {
        while (true) {
            storage.get();
        }
    }
}
