package com.hillel.archive.lesson10_threads.waitingMonitor.data;

public class Storage {
    private int product;
    boolean valueSet = false;

    public synchronized void put(int product) {
        while (valueSet) {
            try {
                wait();
            } catch (InterruptedException e) {
                System.out.println("InterruptedException caught");
            }
        }
        System.out.println("Put " + product);
        this.product = product;
        valueSet = true;
        notify();
    }

    public synchronized int get() {
        while (!valueSet) {
            try {
                wait();
            } catch (InterruptedException e) {
                System.out.println("InterruptedException caught");
            }
        }
        System.out.println("Got " + product);
        valueSet = false;
        notify();
        return product;
    }
}
