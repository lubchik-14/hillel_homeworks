package com.hillel.archive.lesson10_threads.waitingMonitor;

import com.hillel.archive.lesson10_threads.waitingMonitor.data.Storage;
import com.hillel.archive.lesson10_threads.waitingMonitor.thread.Customer;
import com.hillel.archive.lesson10_threads.waitingMonitor.thread.Producer;

public class PutGet {
    public static void main(String[] args) {
        Storage storage = new Storage();

        new Producer(storage);
        new Customer(storage);


    }
}
