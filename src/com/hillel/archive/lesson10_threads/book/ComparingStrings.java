package com.hillel.archive.lesson10_threads.book;

public class ComparingStrings {
    public static void main(String[] args) {
        String s1 = "23";
        String s2 = new String(s1);
        System.out.println(s2);
        Cat cat = new Cat();
        Cat cat1 = new Cat("Vasa");
        System.out.println(cat.name);
        System.out.println(cat1.name);
    }
}

class Cat {
    String name = "Paha";

    Cat() {}

    Cat(String name) {
        this.name = name;
    }
}
