package com.hillel.archive.lesson10_threads.book;

public class HomeTraining {
    public static void main(String[] args) {
        Runtime rt = Runtime.getRuntime();
        System.out.println("Полный объем памяти: " + rt.totalMemory());
        System.out.println("Свободная память: " + rt.freeMemory());
        double d[] = new double[9999999];
        System.out.println("Свободная память после" + " объявления массива: " + rt.freeMemory());
        ProcessBuilder pb = new ProcessBuilder("mspaint", "d:\\installs\\x_18.jpg");

        try {
            pb.start();
        } catch (java.io.IOException e) {
            System.err.println(e.getMessage());
        }
        System.out.println("Свободная память после " + "запуска mspaint.exe: " + rt.freeMemory());
        System.out.println("Список команд: " + pb.command());

    }
}