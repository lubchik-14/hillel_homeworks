package com.hillel.archive.hw8.data;
import com.hillel.archive.hw8.unit.PartOfRange;

import java.util.Arrays;

public class Storage {
    private volatile int[] primeNumbers;

    public Storage() {
        this.primeNumbers = new int[0];
    }

    public synchronized void appendToStorage(int[] partOfPrimeNumbers, PartOfRange partOfRange, int id) {
        primeNumbers = Arrays.copyOf(primeNumbers, (primeNumbers.length + partOfPrimeNumbers.length));
        for (int i = 0; i < partOfPrimeNumbers.length; i++) {
            primeNumbers[primeNumbers.length - partOfPrimeNumbers.length + i] = partOfPrimeNumbers[i];
        }
        System.out.println(id + " " + (partOfRange.getMax() - partOfRange.getMin() + 1) + " (" + partOfRange.getMin() + ":" + partOfRange.getMax() + ") is done");
    }

    public void print() {
        for (int i = 0; i < primeNumbers.length; i++) {
            if (i%30 == 0) {
                System.out.println();
            }
            System.out.print(primeNumbers[i] + ", ");
        }
        System.out.println();
    }

    public int[] getPrimeNumbers() {
        return primeNumbers;
    }

    public void setPrimeNumbers(int[] primeNumbers) {
        this.primeNumbers = primeNumbers;
    }
}
