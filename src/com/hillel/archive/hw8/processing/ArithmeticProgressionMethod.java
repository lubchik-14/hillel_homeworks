package com.hillel.archive.hw8.processing;

import com.hillel.archive.hw8.unit.PartOfRange;

public class ArithmeticProgressionMethod {
    private int min;
    private int max;
    private int numberOfThreads;
    private PartOfRange[] partsOfRange;

    public ArithmeticProgressionMethod(int min, int max, int numberOfThreads) {
        this.min = min;
        this.max = max;
        this.numberOfThreads = numberOfThreads;
        partsOfRange = new PartOfRange[numberOfThreads];
        breakDown();
    }

    private void breakDown() {
        int minL = min;
        int maxL = minL;
        int numberOfValues = max - min + 1;
        double sumProgression;
        double minD = min * 0.000001;
        double maxD = max * 0.000001;
        sumProgression = ((minD + maxD) * numberOfValues / 2) / numberOfThreads;
        sumProgression *= 1000000;
        for (int i = 0; i < numberOfThreads - 1; i++) {
            double a = 1;
            double b = (2 * minL) - 1;
            double c = -1 * 2 * sumProgression;
            double d = Math.pow(b, 2) - 4 * a * c;
            double x = ((-1 * b) + Math.sqrt(d)) / 2;
            maxL = minL + (int) Math.ceil(x) - 1;
            partsOfRange[i] = new PartOfRange(minL, maxL);
            minL = maxL + 1;
        }
        partsOfRange[partsOfRange.length - 1] = new PartOfRange(minL, max);
    }


    public void printPrimeNumbers() {
        for (int i = 0; i < partsOfRange.length; i++) {
            System.out.println((partsOfRange[i].getMax() - partsOfRange[i].getMin() + 1) + "  " + partsOfRange[i].getMin() + " : " + partsOfRange[i].getMax() + ",  ");
        }
    }

    public int getMin() {
        return min;
    }

    public void setMin(int min) {
        this.min = min;
    }

    public int getMax() {
        return max;
    }

    public void setMax(int max) {
        this.max = max;
    }

    public int getNumberOfThreads() {
        return numberOfThreads;
    }

    public void setNumberOfThreads(int numberOfThreads) {
        this.numberOfThreads = numberOfThreads;
    }

    public PartOfRange[] getPartsOfRange() {
        return partsOfRange;
    }

    public void setPartsOfRange(PartOfRange[] partsOfRange) {
        this.partsOfRange = partsOfRange;
    }
}
