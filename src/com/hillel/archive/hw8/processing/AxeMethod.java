package com.hillel.archive.hw8.processing;

import com.hillel.archive.hw8.unit.PartOfRange;

public class AxeMethod {
    private int min;
    private int max;
    private int numberOfThreads;
    private PartOfRange[] partsOfRange;

    public AxeMethod(int min, int max, int numberOfThreads) {
        this.min = min;
        this.max = max;
        this.numberOfThreads = numberOfThreads;
        partsOfRange = new PartOfRange[numberOfThreads];
        breakDown();
    }

    private void breakDown() {

        int minL = min;
        int maxL = minL;
        int numberOfValues = max - min + 1;
        for (int i = 0; i < numberOfThreads; i++) {
            if (numberOfValues > (Math.ceil((max - min) / (Math.pow(2, (i + 1))) + (numberOfThreads - i + 1)))) {
                maxL += Math.ceil(((max - min)) / (Math.pow(2, (i + 1))));
            } else {
                maxL = minL + ((numberOfValues) - (numberOfThreads - i));
            }
            partsOfRange[i] = new PartOfRange(minL, maxL);
            minL = maxL + 1;
            numberOfValues = max - maxL;
        }
    }

    public void printPrimeNumbers() {
        for (int i = 0; i < partsOfRange.length; i++) {
                System.out.println((partsOfRange[i].getMax() - partsOfRange[i].getMin() + 1) + "  " + partsOfRange[i].getMin() + " : " + partsOfRange[i].getMax() + ",  ");
        }
    }

    public int getMin() {
        return min;
    }

    public void setMin(int min) {
        this.min = min;
    }

    public int getMax() {
        return max;
    }

    public void setMax(int max) {
        this.max = max;
    }

    public int getNumberOfThreads() {
        return numberOfThreads;
    }

    public void setNumberOfThreads(int numberOfThreads) {
        this.numberOfThreads = numberOfThreads;
    }

    public PartOfRange[] getPartsOfRange() {
        return partsOfRange;
    }

    public void setPartsOfRange(PartOfRange[] partsOfRange) {
        this.partsOfRange = partsOfRange;
    }
}
