package com.hillel.archive.hw8.processing;

import com.hillel.archive.hw8.thread.PrimeThread;
import com.hillel.archive.hw8.data.Storage;

public class ThreadManager {
    private PrimeThread[] primeThreads = {};
    private AxeMethod axeMethod;
    private ArithmeticProgressionMethod arithmeticProgressionMethod;
    private Storage storage;

    public ThreadManager(AxeMethod method, Storage storage) {
        this.primeThreads = new PrimeThread[method.getNumberOfThreads()];
        this.axeMethod = method;
        this.storage = storage;
    }

    public ThreadManager(ArithmeticProgressionMethod method, Storage storage) {
        this.primeThreads = new PrimeThread[method.getNumberOfThreads()];
        this.arithmeticProgressionMethod = method;
        this.storage = storage;
    }

    public void startAxe() {
        for (int i = 0; i < axeMethod.getNumberOfThreads(); i++) {
            primeThreads[i] = new PrimeThread(axeMethod.getPartsOfRange()[i], storage, i);
        }

        try {
            for (int i = 0; i < axeMethod.getNumberOfThreads(); i++) {
                primeThreads[i].getT().join();
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void startAr() {
        for (int i = 0; i < arithmeticProgressionMethod.getNumberOfThreads(); i++) {
            primeThreads[i] = new PrimeThread(arithmeticProgressionMethod.getPartsOfRange()[i], storage, i);
        }

        try {
            for (int i = 0; i < arithmeticProgressionMethod.getNumberOfThreads(); i++) {
                primeThreads[i].getT().join();
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
