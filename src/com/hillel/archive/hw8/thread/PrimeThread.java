package com.hillel.archive.hw8.thread;

import com.hillel.archive.hw8.unit.PartOfRange;
import com.hillel.archive.hw8.data.Storage;

import java.util.Arrays;

public class PrimeThread implements Runnable {
    private final int id;
    private Thread t;
    private Storage storage;
    private PartOfRange partOfRange;
    private int[] foundPrimeNumbers = {};

    public PrimeThread(PartOfRange partOfRange, Storage storage, int id) {
        this.storage = storage;
        this.partOfRange = partOfRange;
        this.t = new Thread(this);
        this.id = id;
        t.start();
    }

    public void run() {
        for (int i = partOfRange.getMin(); i <= partOfRange.getMax(); i++) {
            if (isPrime(i)) {
                appendToArray(i);
            }
        }
        storage.appendToStorage(foundPrimeNumbers, partOfRange, id);
    }

    public boolean isPrime(int value) {
        if (value < 2) {
            return false;
        }
        if ((value == 2) || (value == 3)) {
            return true;
        }
        for (int i = 2; i < value - 1; i++) {
            if (value % i == 0) {
                return false;
            }
        }
        return true;
    }

    public void appendToArray(int value) {
        foundPrimeNumbers = Arrays.copyOf(foundPrimeNumbers, foundPrimeNumbers.length + 1);
        foundPrimeNumbers[foundPrimeNumbers.length - 1] = value;
    }

    public int[] getFoundPrimeNumbers() {
        return foundPrimeNumbers;
    }

    public void setFoundPrimeNumbers(int[] foundPrimeNumbers) {
        this.foundPrimeNumbers = foundPrimeNumbers;
    }

    public Thread getT() {
        return t;
    }

    public void setT(Thread t) {
        this.t = t;
    }
}
