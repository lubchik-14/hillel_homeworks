package com.hillel.archive.hw8;

import com.hillel.archive.hw8.data.Storage;
import com.hillel.archive.hw8.processing.ArithmeticProgressionMethod;
import com.hillel.archive.hw8.processing.ThreadManager;

public class HW8 {
    public static void main(String[] args) {
        long start;
        long finish;

//        start = System.currentTimeMillis();
//        AxeMethod axeMethod = new AxeMethod(1, 99999, 15);
//        finish = System.currentTimeMillis();
//        System.out.println("Took to break down " + (finish - start) + "ms.");
//        Storage storage = new Storage();
//        ThreadManager threadManager = new ThreadManager(axeMethod, storage);
//        threadManager.startAxe();
//        finish = System.currentTimeMillis();
//        System.out.println("Took the whole method " + (finish - start) + "ms.");


        start = System.currentTimeMillis();
        ArithmeticProgressionMethod arithmeticProgressionMethod = new ArithmeticProgressionMethod(1, 100, 8);
        finish = System.currentTimeMillis();
        System.out.println("Took to break down " + (finish - start) + "ms.");
        Storage storage1 = new Storage();
        ThreadManager threadManager1 = new ThreadManager(arithmeticProgressionMethod, storage1);
        threadManager1.startAr();
        finish = System.currentTimeMillis();
        System.out.println("Took the whole method " + (finish - start) + "ms.");

        arithmeticProgressionMethod.printPrimeNumbers();




    }
}
