package com.hillel.archive.hw6.HW6.src.hw6.interfaces.vehicle;

public interface Vehicle {
    void makeNoise();
    void move();
    void pollute();
    String getBrand();
    double getCost();
    double getFuelEconomy();
    int getMaxSpeed();
    String getModel();
    void setBrand(String brand);
    void setCost(double cost);
    void setFuelEconomy(double fuelEconomy);
    void setMaxSpeed(int maxSpeed);
    void setModel(String model);
}
