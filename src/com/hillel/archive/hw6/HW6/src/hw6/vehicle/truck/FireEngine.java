package com.hillel.archive.hw6.HW6.src.hw6.vehicle.truck;

import com.hillel.archive.hw6.HW6.src.hw6.interfaces.vehicle.forCargo.AbleToFightFire;

public class FireEngine extends Truck implements AbleToFightFire {
    private double limitOfWaterVolume;
    private int quantityOfLights;
    private int quantityOfLadders;

    public FireEngine(int id, String brand, String model, double fuelEconomy, int maxSpeed, double cost,
                      boolean canUseTrailer, double maxWeightLimit, double limitOfWaterVolume, int quantityOfLights,
                      int quantityOfLadders) {
        super(id, brand, model, fuelEconomy, maxSpeed, cost, canUseTrailer, maxWeightLimit);
        this.limitOfWaterVolume = limitOfWaterVolume;
        this.quantityOfLights = quantityOfLights;
        this.quantityOfLadders = quantityOfLadders;
    }

    public void turnOnPomp() {
        System.out.println("Loading water...");
    }

    public void turnOffPomp() {
        System.out.println("Stop loading water...");
    }

    public void fightFire() {
        System.out.println("Fighting a fire...");
    }

    @Override
    public String toString() {
        return "Fire Engine\t\t\t" + super.toString()
                + limitOfWaterVolume + ",\t"
                + quantityOfLights + " lights, \t" +
                quantityOfLadders + " ladders";
    }

    public double getLimitOfWaterVolume() {
        return limitOfWaterVolume;
    }

    public void setLimitOfWaterVolume(double limitOfWaterVolume) {
        this.limitOfWaterVolume = limitOfWaterVolume;
    }

    public int getQuantityOfLights() {
        return quantityOfLights;
    }

    public void setQuantityOfLights(int quantityOfLights) {
        this.quantityOfLights = quantityOfLights;
    }

    public int getQuantityOfLadders() {
        return quantityOfLadders;
    }

    public void setQuantityOfLadders(int quantityOfLadders) {
        this.quantityOfLadders = quantityOfLadders;
    }
}