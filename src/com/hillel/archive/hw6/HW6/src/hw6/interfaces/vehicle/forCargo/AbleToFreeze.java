package com.hillel.archive.hw6.HW6.src.hw6.interfaces.vehicle.forCargo;

public interface AbleToFreeze extends AbleToHaulCargo{
    void turnOnFreezing();
    void turnOffFreezing();
    void increaseTemperature(int pointToIncrease);
    void lowerTemperature(int pointToLow);
    int getQuantityOfChambers();
    void setQuantityOfChambers(int quantityOfChambers);
    double getMinTemperature();
    void setMinTemperature(double minTemperature);
    double getMaxTemperature();
    void setMaxTemperature(double maxTemperature);
}
