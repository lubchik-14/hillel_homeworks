package com.hillel.archive.hw6.HW6.src.hw6.interfaces.vehicle.forCargo;

public interface AbleToFightFire extends AbleToHaulCargo{
    void turnOnPomp();
    void turnOffPomp();
    void fightFire();
    double getLimitOfWaterVolume();
    void setLimitOfWaterVolume(double limitOfWaterVolume);
    int getQuantityOfLights();
    void setQuantityOfLights(int quantityOfLights);
    int getQuantityOfLadders();
    void setQuantityOfLadders(int quantityOfLadders);
}
