package com.hillel.archive.hw6.HW6.src.hw6.vehicleFleet;

import com.hillel.archive.hw6.HW6.src.hw6.interfaces.vehicleFleet.VehicleFleet;
import com.hillel.archive.hw6.HW6.src.hw6.vehicle.EngineVehicle;

public class LubasVehicleFleet implements VehicleFleet {
    private EngineVehicle[] engineVehicles;

    public LubasVehicleFleet(EngineVehicle[] engineVehicles) {
        this.engineVehicles = engineVehicles;
    }

    @Override
    public double countCosts() {
        double sum = 0;
        for (EngineVehicle i : engineVehicles) {
            sum += i.getCost();
        }
        return sum;
    }

    @Override
    public void sortByFuelEconomy() {
        EngineVehicle buff;
        for (int j = 1; j < engineVehicles.length - 1; j++) {
            int countExchanges = 0;
            for (int i = 0; i < engineVehicles.length - j; i++) {
                if (engineVehicles[i].getFuelEconomy() > engineVehicles[i + 1].getFuelEconomy()) {
                    countExchanges = 1;
                    buff = engineVehicles[i];
                    engineVehicles[i] = engineVehicles[i + 1];
                    engineVehicles[i + 1] = buff;
                }
            }
            if (countExchanges == 0) {
                break;
            }
        }
    }

    @Override
    public EngineVehicle[] findBySpeedRange(int minSpeed, int maxSpeed) {
        EngineVehicle[] foundBiggerThan = new EngineVehicle[0];
        for (EngineVehicle engineVehicle : engineVehicles) {
            if ((engineVehicle.getMaxSpeed() >= minSpeed) && (engineVehicle.getMaxSpeed() <= maxSpeed)) {
                EngineVehicle[] found = foundBiggerThan;
                foundBiggerThan = new EngineVehicle[foundBiggerThan.length + 1];
                for (int i = 0; i < found.length; i++) {
                    foundBiggerThan[i] = found[i];
                }
                foundBiggerThan[foundBiggerThan.length - 1] = engineVehicle;
            }
        }
        return foundBiggerThan;
    }

    @Override
    public String toString() {
        String printout = "";
        for (EngineVehicle engineVehicle : engineVehicles) {
            printout += engineVehicle.toString() + "\n";
        }
        return printout;
    }

    public String printArray(EngineVehicle[] vehiclesForPrinting) {
        String printout = "";
        for (EngineVehicle engineVehicle : vehiclesForPrinting) {
            printout += engineVehicle.toString() + "\n";
        }
        return printout;
    }
}
