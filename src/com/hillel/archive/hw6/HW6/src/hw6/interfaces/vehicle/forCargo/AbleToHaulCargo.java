package com.hillel.archive.hw6.HW6.src.hw6.interfaces.vehicle.forCargo;

public interface AbleToHaulCargo {
    void loadCargo();
    void hitchTrailer();
    void unHitchTrailer();
    boolean isCanUseTrailer();
    void setCanUseTrailer(boolean canUseTrailer);
    double getMaxWeightLimit();
    void setMaxWeightLimit(double maxWeightLimit);
}
