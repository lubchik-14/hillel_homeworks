package com.hillel.archive.hw6.HW6.src.hw6.vehicle.bus;

import com.hillel.archive.hw6.HW6.src.hw6.interfaces.vehicle.forPeople.PublicTransport;

public class CityBus extends Bus implements PublicTransport {
    private int quantityOfDisabled;
    private int quantityOfCeilingMountedHandles;

    public CityBus(int id, String brand, String model, double fuelEconomy, int maxSpeed, double cost, int totalNumberOfSeats,
                   int quantityOfDisabled, int quantityOfCeilingMountedHandles) {
        super(id, brand, model, fuelEconomy, maxSpeed, cost, totalNumberOfSeats);
        this.quantityOfDisabled = quantityOfDisabled;
        this.quantityOfCeilingMountedHandles = quantityOfCeilingMountedHandles;
    }

    public void announceBySpeakerphone() {
        System.out.println("Announcing the next bus stop");
    }

    @Override
    public String toString() {
        return "City Bus\t\t\t" + super.toString()
                + quantityOfDisabled + " rooms for disabled people, \t"
                + quantityOfCeilingMountedHandles + " ceiling mounted handles";
    }

    public int getQuantityOfDisabled() {
        return quantityOfDisabled;
    }

    public int getQuantityOfCeilingMountedHandles() {
        return quantityOfCeilingMountedHandles;
    }

    public void setQuantityOfCeilingMountedHandles(int quantityOfCeilingMountedHandles) {
        this.quantityOfCeilingMountedHandles = quantityOfCeilingMountedHandles;
    }

    public void setQuantityOfDisabled(int quantityOfDisabled) {
        this.quantityOfDisabled = quantityOfDisabled;
    }
}