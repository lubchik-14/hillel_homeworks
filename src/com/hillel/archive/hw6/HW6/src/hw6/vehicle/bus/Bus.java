package com.hillel.archive.hw6.HW6.src.hw6.vehicle.bus;

import com.hillel.archive.hw6.HW6.src.hw6.interfaces.vehicle.forPeople.AbleToTransportPeople;
import com.hillel.archive.hw6.HW6.src.hw6.vehicle.EngineVehicle;

public abstract class Bus extends EngineVehicle implements AbleToTransportPeople {
    private int passengerCapacity;

    Bus(int id, String brand, String model, double fuelEconomy, int maxSpeed, double cost, int passengerCapacity) {
        super(id, brand, model, fuelEconomy, maxSpeed, cost);
        this.passengerCapacity = passengerCapacity;
    }

    public void loadPassengers() {
        System.out.println("Doors are opening and passengers are coming in " + getBrand() + " " + getModel());
    }

    @Override
    public String toString() {
        return super.toString()
                + passengerCapacity + " seats,\t";
    }

    public int getPassengerCapacity() {
        return passengerCapacity;
    }

    public void setPassengerCapacity(int passengerCapacity) {
        this.passengerCapacity = passengerCapacity;
    }
}
