package com.hillel.archive.hw6.HW6.src.hw6.interfaces.vehicle.forPeople;

public interface PublicTransport extends AbleToTransportPeople {
    void announceBySpeakerphone();
    int getQuantityOfDisabled();
    int getQuantityOfCeilingMountedHandles();
    void setQuantityOfDisabled(int quantityOfDisabled);
    void setQuantityOfCeilingMountedHandles(int quantityOfCeilingMountedHandles);
}
