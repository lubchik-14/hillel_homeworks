package com.hillel.archive.hw6.HW6.src.hw6.vehicle.truck;

import com.hillel.archive.hw6.HW6.src.hw6.interfaces.vehicle.forCargo.AbleToMix;

public class ConcreteMixer extends Truck implements AbleToMix {
    private double maxVolumeOfMixture;

    public ConcreteMixer(int id, String brand, String model, double fuelEconomy, int maxSpeed, double cost,
                         boolean canUseTrailer, double maxWeightLimit, double maxVolumeOfMixture) {
        super(id, brand, model, fuelEconomy, maxSpeed, cost, canUseTrailer, maxWeightLimit);
        this.maxVolumeOfMixture = maxVolumeOfMixture;
    }

    public void turnOnMixer() {
        System.out.println("The mixer is turned on");
    }

    public void turnOffMixer() {
        System.out.println("The mixer is turned off");
    }

    public void increaseRate(int pointToIncrease) {
        System.out.println("Speed of spin is increased on " + pointToIncrease);
    }

    public void lowerRate(int pointToLow) {
        System.out.println("Speed of spin is lowered on " + pointToLow);
    }

    @Override
    public String toString() {
        return "Concrete Mixer\t\t" + super.toString()
                + maxVolumeOfMixture + "limit of volume";
    }

    public double getMaxVolumeOfMixture() {
        return maxVolumeOfMixture;
    }

    public void setMaxVolumeOfMixture(double maxVolumeOfMixture) {
        this.maxVolumeOfMixture = maxVolumeOfMixture;
    }

}
