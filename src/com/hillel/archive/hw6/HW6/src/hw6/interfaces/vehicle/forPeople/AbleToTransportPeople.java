package com.hillel.archive.hw6.HW6.src.hw6.interfaces.vehicle.forPeople;

public interface AbleToTransportPeople {
    void loadPassengers();
    int getPassengerCapacity();
    void setPassengerCapacity(int passengerCapacity);
}
