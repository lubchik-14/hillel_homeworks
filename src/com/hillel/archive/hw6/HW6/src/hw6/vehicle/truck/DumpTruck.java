package com.hillel.archive.hw6.HW6.src.hw6.vehicle.truck;

public class DumpTruck extends Truck {
    private boolean isLeftOrRightGate;

    public DumpTruck(int id, String brand, String model, double fuelEconomy, int maxSpeed, double cost,
                     boolean canUseTrailer, double maxWeightLimit, boolean isLeftOrRightGate) {
        super(id, brand, model, fuelEconomy, maxSpeed, cost, canUseTrailer, maxWeightLimit);
        this.isLeftOrRightGate = isLeftOrRightGate;
    }

    @Override
    public String toString() {
        return "Dump Truck\t\t\t" + super.toString()
                + ",\tright/left opening: " + isLeftOrRightGate;
    }

    public boolean isLeftOrRightGate() {
        return isLeftOrRightGate;
    }

    public void setLeftOrRightGate(boolean leftOrRightGate) {
        isLeftOrRightGate = leftOrRightGate;
    }
}