package com.hillel.archive.hw6.HW6.src.hw6.vehicle.bus;

public class SchoolBus extends Bus {
    private int quantityOfSeatBelts;
    private int quantityOfEmergencyExits;

    public SchoolBus(int id, String brand, String model, double fuelEconomy, int maxSpeed, double cost, int totalNumberOfSeats,
                     int quantityOfSeatBelts, int quantityOfEmergencyExits) {
        super(id, brand, model, fuelEconomy, maxSpeed, cost, totalNumberOfSeats);
        this.quantityOfSeatBelts = quantityOfSeatBelts;
        this.quantityOfEmergencyExits = quantityOfEmergencyExits;
    }

    @Override
    public String toString() {
        return "School Bus\t\t\t" + super.toString()
                + quantityOfSeatBelts + " seat belts,\t"
                + quantityOfEmergencyExits + " emergency exits ";
    }

    public int getQuantityOfSeatBelts() {
        return quantityOfSeatBelts;
    }

    public void setQuantityOfSeatBelts(int quantityOfSeatBelts) {
        this.quantityOfSeatBelts = quantityOfSeatBelts;
    }

    public int getQuantityOfEmergencyExits() {
        return quantityOfEmergencyExits;
    }

    public void setQuantityOfEmergencyExits(int quantityOfEmergencyExits) {
        this.quantityOfEmergencyExits = quantityOfEmergencyExits;
    }
}
