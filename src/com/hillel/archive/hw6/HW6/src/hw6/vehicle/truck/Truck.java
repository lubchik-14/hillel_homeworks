package com.hillel.archive.hw6.HW6.src.hw6.vehicle.truck;

import com.hillel.archive.hw6.HW6.src.hw6.interfaces.vehicle.forCargo.AbleToHaulCargo;
import com.hillel.archive.hw6.HW6.src.hw6.vehicle.EngineVehicle;

public abstract class Truck extends EngineVehicle implements AbleToHaulCargo {
    private boolean canUseTrailer;
    private double maxWeightLimit;

    Truck(int id, String brand, String model, double fuelEconomy, int maxSpeed, double cost, boolean canUseTrailer,
          double maxWeightLimit) {
        super(id, brand, model, fuelEconomy, maxSpeed, cost);
        this.canUseTrailer = canUseTrailer;
        this.maxWeightLimit = maxWeightLimit;
    }

    public void pollute() {
        System.out.println("Making much more smog...");
    }

    public void loadCargo() {
        System.out.println("Loading cargo...");
    }

    public void hitchTrailer() {
        System.out.println("The trailer is hitched");
    }

    public void unHitchTrailer() {
        System.out.println("The trailer is unhitched");
    }

    @Override
    public String toString() {
        return super.toString()
                + "using a trailer: " + canUseTrailer + ",\t"
                + maxWeightLimit + " max weight limit,\t";
    }

    public boolean isCanUseTrailer() {
        return canUseTrailer;
    }

    public void setCanUseTrailer(boolean canUseTrailer) {
        this.canUseTrailer = canUseTrailer;
    }

    public double getMaxWeightLimit() {
        return maxWeightLimit;
    }

    public void setMaxWeightLimit(double maxWeightLimit) {
        this.maxWeightLimit = maxWeightLimit;
    }
}
