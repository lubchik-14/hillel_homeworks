package com.hillel.archive.hw6.HW6.src.hw6.interfaces.vehicle.forPeople;

public interface PublicTransportForLongDistance extends AbleToTransportPeople {
    int makeSandwiches();
    void turnOffAirConditioner();
    int getQuantityOfTvs();
    int getQuantityOfFloors();
    void setQuantityOfTvs(int quantityOfTvs);
    void setQuantityOfFloors(int quantityOfFloors);
}
