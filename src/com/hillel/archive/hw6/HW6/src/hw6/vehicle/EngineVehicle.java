package com.hillel.archive.hw6.HW6.src.hw6.vehicle;

import com.hillel.archive.hw6.HW6.src.hw6.interfaces.vehicle.Vehicle;

public abstract class EngineVehicle implements Vehicle {
    private int id;
    private int maxSpeed;
    private double cost;
    private double fuelEconomy;
    private String brand;
    private String model;

    public EngineVehicle(int id, String brand, String model, double fuelEconomy, int maxSpeed, double cost) {
        this.id = id;
        this.brand = brand;
        this.model = model;
        this.maxSpeed = maxSpeed;
        this.cost = cost;
        this.fuelEconomy = fuelEconomy;
    }

    public void makeNoise() {
        System.out.println("Beep! Beep!");
    }

    public void move() {
        System.out.println(this.brand + " " + this.model + " is moving...");
    }

    public void pollute() {
        System.out.println(this.brand + " " + this.model + "is smogging...");
    }

    @Override
    public String toString() {
        return "ID: " + id + "\t"
                + brand + " "
                + model + ",\t"
                + fuelEconomy + " km/L,\t"
                + maxSpeed + " km an hour,\t"
                + "$ " + cost + ",\t";
    }
    public String getBrand() {
        return brand;
    }

    public double getCost() {
        return cost;
    }

    public double getFuelEconomy() {
        return fuelEconomy;
    }

    public int getMaxSpeed() {
        return maxSpeed;
    }

    public String getModel() {
        return model;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public void setCost(double cost) {
        this.cost = cost;
    }

    public void setFuelEconomy(double fuelEconomy) {
        this.fuelEconomy = fuelEconomy;
    }

    public void setMaxSpeed(int maxSpeed) {
        this.maxSpeed = maxSpeed;
    }

    public void setModel(String model) {
        this.model = model;
    }
}
