package com.hillel.archive.hw6.HW6.src.hw6.interfaces.vehicle.forCargo;

public interface AbleToMix extends AbleToHaulCargo{
    void turnOnMixer();
    void turnOffMixer();
    void increaseRate(int pointToIncrease);
    void lowerRate(int pointToLow);
    double getMaxVolumeOfMixture();
    void setMaxVolumeOfMixture(double maxVolumeOfMixture);
}
