package com.hillel.archive.hw6.HW6.src.hw6;

import com.hillel.archive.hw6.HW6.src.hw6.vehicle.EngineVehicle;
import com.hillel.archive.hw6.HW6.src.hw6.vehicle.bus.CityBus;
import com.hillel.archive.hw6.HW6.src.hw6.vehicle.bus.SchoolBus;
import com.hillel.archive.hw6.HW6.src.hw6.vehicle.bus.SkyLiner;
import com.hillel.archive.hw6.HW6.src.hw6.vehicle.truck.ConcreteMixer;
import com.hillel.archive.hw6.HW6.src.hw6.vehicle.truck.DumpTruck;
import com.hillel.archive.hw6.HW6.src.hw6.vehicle.truck.FireEngine;
import com.hillel.archive.hw6.HW6.src.hw6.vehicle.truck.Refrigerator;
import com.hillel.archive.hw6.HW6.src.hw6.vehicleFleet.LubasVehicleFleet;

public class HW6 {
    public static void main(String[] args) {
        EngineVehicle[] engineVehicles = new EngineVehicle[]{
                new DumpTruck(1, "GINAF", "G 5450", 100, 70, 50000,
                        true, 31000, true),
                new CityBus(2, "Богдан", "А601.10", 24, 80, 115375.99,
                        27, 0, 10),
                new SkyLiner(3, "Neoplan", "N316 SHD", 12.4, 128, 133864,
                        52, 4, 2),
                new ConcreteMixer(4, "Renault", "Kerax 385", 18, 90, 35000,
                        false, 3500, 10000),
                new CityBus(5, "ПАЗ", "32054", 15, 95, 48999.99,
                        20, 1, 0),
                new DumpTruck(6, "БЕЛАЗ", "75710", 1300, 67, 10000000,
                        false, 450000, false),
                new FireEngine(7, "ГАЗ", "2705 АПП-4-276", 60, 170, 40000,
                        true, 2000, 2500, 30,
                        1),
                new Refrigerator(8, "Renault", "Premium", 27.5, 90, 25000,
                        true, 2100, 1, -20, +20),
                new SkyLiner(9, "Mercedes-Benz", "Tourismo L", 15, 150, 150000,
                        55, 10, 1),
                new SchoolBus(10, "IC", "Nexbus XL", 8.8, 100, 175000,
                        57, 56, 3),
                new ConcreteMixer(11, "КамАЗ", "53229", 20, 85, 20000,
                        false, 2500, 7000),
                new SchoolBus(12, "Thomas Built Buses", "Freightliner C2", 16, 130,
                        215000, 50, 50, 2),
                new Refrigerator(13, "Ford", "Transit", 30, 100, 20000,
                        true, 2000, 4, -30, +22),
                new FireEngine(14, "ЗИЛ", "АНР-40-1400", 50, 150, 59999.99,
                        false, 2500, 20000, 25,
                        3)
        };

        LubasVehicleFleet lubasVehicleFleet = new LubasVehicleFleet(engineVehicles);
        System.out.println("Luba's VehicleFleet costs $" + lubasVehicleFleet.countCosts());
        System.out.println();
        System.out.println("Unsorted Luba's VehicleFleet:");
        System.out.println(lubasVehicleFleet);
        System.out.println();
        lubasVehicleFleet.sortByFuelEconomy();
        System.out.println("Sorted Luba's VehicleFleet:");
        System.out.println(lubasVehicleFleet);
        System.out.println();
        System.out.println("Vehicles that have maxSpeed in the range 110 to 200 km an hour:");
        System.out.println(lubasVehicleFleet.printArray(lubasVehicleFleet.findBySpeedRange(110, 200)));
        System.out.println();
        ((SkyLiner) engineVehicles[1]).loadPassengers();
        ((SkyLiner) engineVehicles[1]).turnOffAirConditioner();
    }
}
