package com.hillel.archive.hw6.HW6.src.hw6.vehicle.bus;

import com.hillel.archive.hw6.HW6.src.hw6.interfaces.vehicle.forPeople.PublicTransportForLongDistance;

public class SkyLiner extends Bus implements PublicTransportForLongDistance {
    private int quantityOfTvs;
    private int quantityOfFloors;

    public SkyLiner(int id, String brand, String model, double fuelEconomy, int maxSpeed, double cost, int totalNumberOfSeats,
                    int quantityOfTvs, int quantityOfFloors) {
        super(id, brand, model, fuelEconomy, maxSpeed, cost, totalNumberOfSeats);
        this.quantityOfTvs = quantityOfTvs;
        this.quantityOfFloors = quantityOfFloors;
    }

    public int makeSandwiches() {
        return getPassengerCapacity();
    }

    public void turnOffAirConditioner() {
        System.out.println("Close windows, please, because of a draught!");
    }

    @Override
    public String toString() {
        return "SkyLiner\t\t\t" + super.toString()
                + quantityOfFloors + " floors,\t"
                + quantityOfTvs + " TVs";
    }

    public int getQuantityOfTvs() {
        return quantityOfTvs;
    }

    public int getQuantityOfFloors() {
        return quantityOfFloors;
    }
    public void setQuantityOfTvs(int quantityOfTvs) {
        this.quantityOfTvs = quantityOfTvs;
    }

    public void setQuantityOfFloors(int quantityOfFloors) {
        this.quantityOfFloors = quantityOfFloors;
    }
}
