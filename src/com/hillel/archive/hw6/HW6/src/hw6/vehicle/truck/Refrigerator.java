package com.hillel.archive.hw6.HW6.src.hw6.vehicle.truck;

import com.hillel.archive.hw6.HW6.src.hw6.interfaces.vehicle.forCargo.AbleToFreeze;

public class Refrigerator extends Truck implements AbleToFreeze {
    private int quantityOfChambers;
    private double minTemperature;
    private double maxTemperature;

    public Refrigerator(int id, String brand, String model, double fuelEconomy, int maxSpeed, double cost,
                        boolean canUseTrailer, double maxWeightLimit, int quantityOfChambers, double minTemperature,
                        double maxTemperature) {
        super(id, brand, model, fuelEconomy, maxSpeed, cost, canUseTrailer, maxWeightLimit);
        this.quantityOfChambers = quantityOfChambers;
        this.minTemperature = minTemperature;
        this.maxTemperature = maxTemperature;
    }

    public void turnOnFreezing() {
        System.out.println("The freezing is turned on");
    }

    public void turnOffFreezing() {
        System.out.println("The freezing is turned off");
    }

    public void increaseTemperature(int pointToIncrease) {
        System.out.println("Temperature is increased on " + pointToIncrease);
    }

    public void lowerTemperature(int pointToLow) {
        System.out.println("Temperature is lowered on " + pointToLow);
    }

    @Override
    public String toString() {
        return "Refrigerator\t\t" + super.toString()
                + quantityOfChambers + " chambers,\t"
                + minTemperature + "/+"
                + maxTemperature + " C\u00b0";
    }

    public int getQuantityOfChambers() {
        return quantityOfChambers;
    }

    public void setQuantityOfChambers(int quantityOfChambers) {
        this.quantityOfChambers = quantityOfChambers;
    }

    public double getMinTemperature() {
        return minTemperature;
    }

    public void setMinTemperature(double minTemperature) {
        this.minTemperature = minTemperature;
    }

    public double getMaxTemperature() {
        return maxTemperature;
    }

    public void setMaxTemperature(double maxTemperature) {
        this.maxTemperature = maxTemperature;
    }
}