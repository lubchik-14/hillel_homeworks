package com.hillel.archive.lesson11_annotations.example_one_paramertr;

import com.hillel.archive.lesson11_annotations.annotation.MyAnnotation;

import java.lang.reflect.Method;

public class Meta {

    @MyAnnotation(str = "Example", value = 1)
    public static void myMethod() {
        Meta meta = new Meta();
        try {
            Class<?> metaClass = meta.getClass();
            Method metaMethod = metaClass.getMethod("myMethod");
            MyAnnotation myAnnotation = metaMethod.getAnnotation(MyAnnotation.class);
            System.out.println(myAnnotation.str() + " " + myAnnotation.value());
        } catch (NoSuchMethodException e) {
            System.out.println("Method 'myMethod not found'");
        }
    }

    public static void main(String[] args) {
        myMethod();
    }
}
