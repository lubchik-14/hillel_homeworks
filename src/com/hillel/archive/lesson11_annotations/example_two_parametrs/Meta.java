package com.hillel.archive.lesson11_annotations.example_two_parametrs;

import com.hillel.archive.lesson11_annotations.annotation.MyAnnotation;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;

@Deprecated
public class Meta {
    @MyAnnotation(str = "Example", value = 2)
    @Deprecated(since = "15/11/22")
    public static void myMethod(String str, int i) {
        Meta meta = new Meta();
        Class metaClass = meta.getClass();
        Annotation[] classAnnotations = metaClass.getAnnotations();
        for (Annotation classAnnotation : classAnnotations) {
            System.out.println(classAnnotation);
        }
        System.out.println();
        try {
            Method metaMethod = metaClass.getMethod("myMethod", String.class, int.class);
            MyAnnotation myAnnotation = metaMethod.getAnnotation(MyAnnotation.class);
            Annotation[] annotations = metaMethod.getAnnotations();
            for (Annotation annotation : annotations) {
                System.out.println(annotation);
            }

            System.out.println(myAnnotation.str() + " " + myAnnotation.value());
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        myMethod("test", 10);
    }
}
