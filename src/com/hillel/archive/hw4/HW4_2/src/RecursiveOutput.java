package com.hillel.archive.hw4.HW4_2.src;

public class RecursiveOutput {
   void printToNumberByRecurs(int num) {
        if (num !=0) {
            printToNumberByRecurs(num - 1);
            System.out.print(num + " ");
        }
    }
}
