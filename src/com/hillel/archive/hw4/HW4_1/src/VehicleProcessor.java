package com.hillel.archive.hw4.HW4_1.src;

import java.time.Year;

public class VehicleProcessor {
    Vehicle[] vehicles;

    VehicleProcessor (Vehicle[] vehicles) {
        this.vehicles = vehicles;
    }

    VehicleProcessor () {
        vehicles = null;
    }

    void printVehicle(Vehicle vehicle) {
            System.out.println(vehicle.id + ": " + vehicle.brand + " " + vehicle.model + " " + vehicle.modelYear + " " +
                    vehicle.color + " " + "$" + vehicle.price + " " + vehicle.licensePlate);
    }

    private void loopByBrand (Vehicle[] vehicles, String brand) {
        for (Vehicle i : vehicles) {
            if (i.brand.equals(brand)) {
                this.printVehicle(i);
            }
        }
    }

    void findByBrand(Vehicle[] vehicles, String brand) {
        loopByBrand(vehicles, brand);
    }

    void findByBrand(String brand) {
        if (vehicles != null) {
            loopByBrand(vehicles, brand);
        } else {
            System.out.println("There is no elements in the array!");
        }
    }

    private void loopByModelAndModelYearGraterThan(Vehicle[] vehicles, String model, int years) {
        for (Vehicle i : vehicles) {
            if ((i.model.equals(model)) && ((Year.now().getValue() - i.modelYear) > years)) {
                this.printVehicle(i);
            }
        }
    }

    void findByModelAndModelYearGraterThan(Vehicle[] vehicles, String model, int years){
        loopByModelAndModelYearGraterThan(vehicles, model, years);
    }

    void findByModelAndModelYearGraterThan(String model, int years){
        if (vehicles != null) {
            loopByModelAndModelYearGraterThan(vehicles, model, years);
        } else {
            System.out.println("There is no elements in the array!");
        }
    }

    private void loopByModelYearAndPriceGrateThan(Vehicle[] vehicles, int modelYear, double price) {
        for (Vehicle i : vehicles) {
            if ((i.modelYear == modelYear) && (i.price > price)) {
                printVehicle(i);            }
        }
    }

    void findByModelYearAndPriceGrateThan(Vehicle[] vehicles, int modelYear, double price) {
        loopByModelYearAndPriceGrateThan(vehicles, modelYear, price);
    }

    void findByModelYearAndPriceGrateThan(int modelYear, double price) {
        if (vehicles != null) {
            loopByModelYearAndPriceGrateThan(vehicles, modelYear, price);
        } else {
            System.out.println("There is no elements in the array!");
        }
    }
}
