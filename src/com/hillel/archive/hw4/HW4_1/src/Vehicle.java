package com.hillel.archive.hw4.HW4_1.src;

public class Vehicle {
    int id;
    String brand;
    String model;
    int modelYear;
    String color;
    double price;
    String licensePlate;

    Vehicle (int id, String brand, String model, int modelYear, String color, double price, String licensePlate) {
        this.id = id;
        this.brand = brand;
        this.model = model;
        this.modelYear = modelYear;
        this.color = color;
        this.price = price;
        this.licensePlate = licensePlate;
    }
}
