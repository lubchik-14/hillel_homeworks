package com.hillel.archive.hw4.HW4_1.src;

public class HW4_1 {
    public static void main (String args[]) {
        Vehicle[] arrayOfVehicles = {
                new Vehicle(1, "BMW", "X5", 2019, "red",555555, "ЕР6666УК"),
                new Vehicle(2, "BMW", "A3", 2010, "maroon", 140000, "ЗЕ6666ПР"),
                new Vehicle(3, "BMW", "330", 2000, "green",44999.99, "ЛЯ6666ЛЯ"),
                new Vehicle(4, "Chevrolet", "Impala", 1958, "purple",100500, "ГВ6666КД"),
                new Vehicle(5, "Chevrolet", "Lacetti", 1990, "orange",79797, "ЧО6666ХО"),
                new Vehicle(6, "Chevrolet", "Aveo", 1984, "black and white",999999.99, "ОЙ6666ОЙ"),
                new Vehicle(7, "Audi", "R8", 2019, "silver",66666, "СИ6666ГА"),
                new Vehicle(8, "Audi", "A3", 2018, "pink",29000, "ЛЮ6666БА"),
                new Vehicle(9, "Audi", "A3", 2000, "yellow",696969, "ХХ6666ХХ"),
                new Vehicle(10, "Mercedes", "S600", 1950, "colorful",100000, "ХВ6666ВВ"),
                new Vehicle(11, "ZAZ", "966", 1967, "dark blue",10000, "АХ9999ХЗ"),
        };
        VehicleProcessor vehicleProcessor = new VehicleProcessor(arrayOfVehicles);
        VehicleProcessor vehicleProcessorDefault = new VehicleProcessor();

        System.out.println("Audi full list");
        vehicleProcessor.findByBrand("Audi");
        vehicleProcessor.findByBrand(arrayOfVehicles, "Audi");
        System.out.println("Audi full list (default constructor)");
        vehicleProcessorDefault.findByBrand("Audi");
        vehicleProcessorDefault.findByBrand(arrayOfVehicles, "Audi");

        System.out.println("Used A3");
        vehicleProcessor.findByModelAndModelYearGraterThan("A3", 5);
        vehicleProcessor.findByModelAndModelYearGraterThan(arrayOfVehicles, "A3", 5);
        System.out.println("Used A3 (default constructor)");
        vehicleProcessorDefault.findByModelAndModelYearGraterThan("A3", 5);
        vehicleProcessorDefault.findByModelAndModelYearGraterThan(arrayOfVehicles, "A3", 5);

        System.out.println("New luxury");
        vehicleProcessor.findByModelYearAndPriceGrateThan(2019, 100000);
        vehicleProcessor.findByModelYearAndPriceGrateThan(arrayOfVehicles, 2019, 100000);
        System.out.println("New luxury (default constructor)");
        vehicleProcessorDefault.findByModelYearAndPriceGrateThan(2019, 100000);
        vehicleProcessorDefault.findByModelYearAndPriceGrateThan(arrayOfVehicles, 2019, 100000);
    }
}

