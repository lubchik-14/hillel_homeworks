package com.hillel.archive.hw4.HW4_3.src;

public class HW4_3 {
    public static void main(String[] args) {
        int num = 1258;
        RecursiveSum recursiveSum = new RecursiveSum();
        System.out.print("Sum digits of number is " + recursiveSum.sumDigitsOfNumberByRecurs(num));
    }
}
