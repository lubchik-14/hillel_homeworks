package com.hillel.archive.hw4.HW4_3.src;

public class RecursiveSum {
    int sum = 0;

    public int sumDigitsOfNumberByRecurs(int num) {
        if (num !=0) {
            sum += num % 10;
            sumDigitsOfNumberByRecurs(num/10);
        }
        return sum;
    }
}
