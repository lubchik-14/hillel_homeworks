package com.hillel.archive.hw4.HW4_4.src;

public class Triangle {

    private Point a, b, c;

    Triangle (Point a, Point b, Point c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }

    public String toString() {
        return "A(" + a.x + ", " + a.y + "), B(" + b.x + ", " + b.y + "), C(" + c.x + ", " + c.y + ")";
    }

    double getAb() {
        return (Math.sqrt(Math.pow((b.x - a.x),2) + Math.pow((b.y - a.y),2)));
    }

    double getBc() {
        return ((Math.sqrt(Math.pow((c.x - b.x),2) + Math.pow((c.y - b.y),2))));
    }

    double getCa() {
        return (Math.sqrt(Math.pow((a.x - c.x),2) + Math.pow((a.y - c.y),2)));
    }

    double getSquare() {
        double p = 0.5 * getPerimeter();
        return (Math.sqrt(p * (p - getAb()) * (p - getBc()) * (p - getCa())));
    }

    double getPerimeter() {
        return getAb() + getBc() + getCa();
    }
    //равносторонний
    boolean isEquilateral() {
        double aB = (double)(Math.round(getAb() * 100000)) / 100000;
        double bC = (double)(Math.round(getBc() * 100000)) / 100000;
        double cA = (double)(Math.round(getCa() * 100000)) / 100000;
        if ((aB == bC) && (bC == cA)) {
            return true;
        }
        return false;
    }
    //прямоуголный
    boolean isRectangular() {
        if ((Math.pow(getAb(),2) == Math.pow(getBc(),2) + Math.pow(getCa(),2)) ||
                (Math.pow(getBc(),2) == Math.pow(getCa(),2) + Math.pow(getAb(),2)) ||
                (Math.pow(getCa(),2) == Math.pow(getAb(),2) + Math.pow(getBc(),2))) {
            return true;
        }
        return false;
    }
    //равнобедренный
    boolean isIsosceles() {
        if (!isEquilateral()) {
            if (((getAb() == getBc()) && (getBc() != getCa()) ||
                    ((getBc() == getCa()) && (getCa() != getAb()))) ||
                    ((getCa() == getAb()) && (getAb() != getBc()))) {
                return true;
            }
        }
        return false;
    }
    //произвольный
    boolean isArbitrary() {
        if (!isIsosceles() && !isRectangular() && !isEquilateral()) {
            return true;
        }
        return false;
    }

    String getType() {
        if (this.isEquilateral()) {
            return "equilateral";
        } else if (this.isIsosceles()) {
            return "isosceles";
        } else if (this.isRectangular()) {
            return "rectangular";
        } else if (this.isArbitrary()) {
            return "arbitrary";
        } else {
            return "unknown";
        }
    }
}
