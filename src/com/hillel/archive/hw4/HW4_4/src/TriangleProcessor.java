package com.hillel.archive.hw4.HW4_4.src;

public class TriangleProcessor {

    Triangle[] triangles;

    TriangleProcessor(Triangle[] triangles) {
        this.triangles = triangles;
    }

    public int getCountEquilateral() {
        int countEquilateral = 0;
        for (Triangle i : triangles) {
            if (i.isEquilateral()) {
                countEquilateral++;
            }
        }
        return countEquilateral;
    }

    public int getCountRectangular(Triangle[] triangles) {
        int countRectangular = 0;
        for (Triangle i : triangles) {
            if (i.isRectangular()) {
                countRectangular++;
            }
        }
        return countRectangular;
    }

    public int getCountIsosceles(Triangle[] triangles) {
        int countIsosceles = 0;
        for (Triangle i : triangles) {
            if (i.isIsosceles()) {
                countIsosceles++;
            }
        }
        return countIsosceles;
    }

    public int getCountArbitrary(Triangle[] triangles) {
        int countArbitrary = 0;
        for (Triangle i : triangles) {
            if (i.isArbitrary()) {
                countArbitrary++;
            }
        }
        return countArbitrary;
    }

    private Triangle findMinBySquare(Triangle[] triangles, String typeOfTriangle) {
        Triangle minValue = null;
        for (Triangle i : triangles)
            if (i.getType().equals(typeOfTriangle)) {
                if (minValue == null) {
                    minValue = i;
                } else if (i.getSquare() < minValue.getSquare()) {
                    minValue = i;
                }
            }
        return minValue;
    }

    private Triangle findMinByPerimeter(Triangle[] triangles, String typeOfTriangle) {
        Triangle minValue = null;
        for (Triangle i : triangles)
            if (i.getType().equals(typeOfTriangle)) {
                if (minValue == null) {
                    minValue = i;
                } else if (i.getPerimeter() < minValue.getPerimeter()) {
                    minValue = i;
                }
            }
        return minValue;
    }

    private Triangle findMaxBySquare(Triangle[] triangles, String typeOfTriangle) {
        Triangle maxValue = null;
        for (Triangle i : triangles)
            if (i.getType().equals(typeOfTriangle)) {
                if (maxValue == null) {
                    maxValue = i;
                } else if (i.getSquare() > maxValue.getSquare()) {
                    maxValue = i;
                }
            }
        return maxValue;
    }

    private Triangle findMaxByPerimeter(Triangle[] triangles, String typeOfTriangle) {
        Triangle maxValue = null;
        for (Triangle i : triangles)
            if (i.getType().equals(typeOfTriangle)) {
                if (maxValue == null) {
                    maxValue = i;
                } else if (i.getPerimeter() > maxValue.getPerimeter()) {
                    maxValue = i;
                }
            }
        return maxValue;
    }

    public Triangle findMinEquilateralBySquare(Triangle[] triangles) {
        return findMinBySquare(triangles, "equilateral");
    }

    public Triangle findMinRectangularBySquare(Triangle[] triangles) {
        return findMinBySquare(triangles, "rectangular");
    }

    public Triangle findMinIsoscelesBySquare(Triangle[] triangles) {
        return findMinBySquare(triangles, "isosceles");
    }

    public Triangle findMinArbitraryBySquare(Triangle[] triangles) {
        return findMinBySquare(triangles, "arbitrary");
    }

    public Triangle findMaxEquilateralBySquare(Triangle[] triangles) {
        return findMaxBySquare(triangles, "equilateral");
    }

    public Triangle findMaxRectangularBySquare(Triangle[] triangles) {
        return findMaxBySquare(triangles, "rectangular");
    }

    public Triangle findMaxIsoscelesBySquare(Triangle[] triangles) {
        return findMaxBySquare(triangles, "isosceles");
    }

    public Triangle findMaxArbitraryBySquare(Triangle[] triangles) {
        return findMaxBySquare(triangles, "arbitrary");
    }

    public Triangle findMinEquilateralByPerimeter(Triangle[] triangles) {
        return findMinByPerimeter(triangles, "equilateral");
    }

    public Triangle findMinRectangularByPerimeter(Triangle[] triangles) {
        return findMinByPerimeter(triangles, "rectangular");
    }

    public Triangle findMinIsoscelesByPerimeter(Triangle[] triangles) {
        return findMinByPerimeter(triangles, "isosceles");
    }

    public Triangle findMinArbitraryByPerimeter(Triangle[] triangles) {
        return findMinByPerimeter(triangles, "arbitrary");
    }

    public Triangle findMaxEquilateralByPerimeter(Triangle[] triangles) {
        return findMaxByPerimeter(triangles, "equilateral");
    }

    public Triangle findMaxRectangularByPerimeter(Triangle[] triangles) {
        return findMaxByPerimeter(triangles, "rectangular");
    }

    public Triangle findMaxIsoscelesByPerimeter(Triangle[] triangles) {
        return findMaxByPerimeter(triangles, "isosceles");
    }

    public Triangle findMaxArbitraryByPerimeter(Triangle[] triangles) {
        return findMaxByPerimeter(triangles, "arbitrary");
    }
}
