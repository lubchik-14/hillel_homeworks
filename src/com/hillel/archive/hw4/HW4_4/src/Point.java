package com.hillel.archive.hw4.HW4_4.src;

public class Point {
    public double x, y;

    Point(double x, double y) {
        this.x = x;
        this.y = y;
    }
}
