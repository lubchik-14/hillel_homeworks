package com.hillel.archive.lesson11_enum.triangle;

class TriangleProcessor {

    Triangle[] triangles;

    TriangleProcessor(Triangle[] triangles) {
        this.triangles = triangles;
    }

    public int getCount(Type type) {
        int count = 0;

        switch (type) {
            case EQUILATERAL:
                for (Triangle i : triangles) {
                    if (i.isEquilateral()) {
                        count++;
                    }
                }
                break;
            case ARBITRARY:
                for (Triangle i : triangles) {
                    if (i.isArbitrary()) {
                        count++;
                    }
                }
                break;
            case ISOSCELES:
                for (Triangle i : triangles) {
                    if (i.isIsosceles()) {
                        count++;
                    }
                }
                break;
            case RECTANGULAR:
                for (Triangle i : triangles) {
                    if (i.isRectangular()) {
                        count++;
                    }
                }
                break;
        }
        return count;
    }

    private Triangle findMinBySquare(Triangle[] triangles, Type typeOfTriangle) {
        Triangle minValue = null;
        for (Triangle i : triangles)
            if (i.getType() == typeOfTriangle) {
                if (minValue == null) {
                    minValue = i;
                } else if (i.getSquare() < minValue.getSquare()) {
                    minValue = i;
                }
            }
        return minValue;
    }

    private Triangle findMinByPerimeter(Triangle[] triangles, Type typeOfTriangle) {
        Triangle minValue = null;
        for (Triangle i : triangles)
            if (i.getType() == typeOfTriangle) {
                if (minValue == null) {
                    minValue = i;
                } else if (i.getPerimeter() < minValue.getPerimeter()) {
                    minValue = i;
                }
            }
        return minValue;
    }

    private Triangle findMaxBySquare(Triangle[] triangles, Type typeOfTriangle) {
        Triangle maxValue = null;
        for (Triangle i : triangles)
            if (i.getType() == typeOfTriangle) {
                if (maxValue == null) {
                    maxValue = i;
                } else if (i.getSquare() > maxValue.getSquare()) {
                    maxValue = i;
                }
            }
        return maxValue;
    }

    private Triangle findMaxByPerimeter(Triangle[] triangles, Type typeOfTriangle) {
        Triangle maxValue = null;
        for (Triangle i : triangles)
            if (i.getType() == typeOfTriangle) {
                if (maxValue == null) {
                    maxValue = i;
                } else if (i.getPerimeter() > maxValue.getPerimeter()) {
                    maxValue = i;
                }
            }
        return maxValue;
    }

    public Triangle findMinEquilateralBySquare(Triangle[] triangles) {
        return findMinBySquare(triangles, Type.EQUILATERAL);
    }

    public Triangle findMinRectangularBySquare(Triangle[] triangles) {
        return findMinBySquare(triangles, Type.RECTANGULAR);
    }

    public Triangle findMinIsoscelesBySquare(Triangle[] triangles) {
        return findMinBySquare(triangles, Type.ISOSCELES);
    }

    public Triangle findMinArbitraryBySquare(Triangle[] triangles) {
        return findMinBySquare(triangles, Type.ARBITRARY);
    }

    public Triangle findMaxEquilateralBySquare(Triangle[] triangles) {
        return findMaxBySquare(triangles, Type.EQUILATERAL);
    }

    public Triangle findMaxRectangularBySquare(Triangle[] triangles) {
        return findMaxBySquare(triangles, Type.RECTANGULAR);
    }

    public Triangle findMaxIsoscelesBySquare(Triangle[] triangles) {
        return findMaxBySquare(triangles, Type.ISOSCELES);
    }

    public Triangle findMaxArbitraryBySquare(Triangle[] triangles) {
        return findMaxBySquare(triangles, Type.ARBITRARY);
    }

    public Triangle findMinEquilateralByPerimeter(Triangle[] triangles) {
        return findMinByPerimeter(triangles, Type.EQUILATERAL);
    }

    public Triangle findMinRectangularByPerimeter(Triangle[] triangles) {
        return findMinByPerimeter(triangles, Type.RECTANGULAR);
    }

    public Triangle findMinIsoscelesByPerimeter(Triangle[] triangles) {
        return findMinByPerimeter(triangles, Type.ISOSCELES);
    }

    public Triangle findMinArbitraryByPerimeter(Triangle[] triangles) {
        return findMinByPerimeter(triangles, Type.ARBITRARY);
    }

    public Triangle findMaxEquilateralByPerimeter(Triangle[] triangles) {
        return findMaxByPerimeter(triangles, Type.EQUILATERAL);
    }

    public Triangle findMaxRectangularByPerimeter(Triangle[] triangles) {
        return findMaxByPerimeter(triangles, Type.RECTANGULAR);
    }

    public Triangle findMaxIsoscelesByPerimeter(Triangle[] triangles) {
        return findMaxByPerimeter(triangles, Type.ISOSCELES);
    }

    public Triangle findMaxArbitraryByPerimeter(Triangle[] triangles) {
        return findMaxByPerimeter(triangles, Type.ARBITRARY);
    }
}
