package com.hillel.archive.lesson11_enum.triangle;

public enum Type {
    EQUILATERAL, RECTANGULAR, ISOSCELES, ARBITRARY, UNDEFINED;
}
