package com.hillel.archive.lesson11_enum.triangle;

class Point {
    double x, y;

    Point(double x, double y) {
        this.x = x;
        this.y = y;
    }
}
