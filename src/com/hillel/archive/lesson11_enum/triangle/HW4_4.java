package com.hillel.archive.lesson11_enum.triangle;

class HW4_4 {
    public static void main(String[] args) {
        Triangle triangle = new Triangle(new Point(0, 0), new Point(0.5, (0.5*Math.sqrt(3))), new Point(1, 0));

        System.out.println("The area of the triangle is " + triangle.getSquare() + "cm\u00B2.");
        System.out.println("The perimeter of the triangle is " + triangle.getPerimeter() + "cm.");

        Triangle[] triangles = new Triangle[]{
                new Triangle(new Point(0, 0), new Point(1, 0), new Point(0.5, (0.5 * Math.sqrt(3)))),   //E
                new Triangle(new Point(2, -3), new Point(1, 1), new Point(-6, 5)),  //A
                new Triangle(new Point(-3, -2), new Point(0, -1), new Point(-2, 5)),    //R
                new Triangle(new Point(2, 3), new Point(6, 7), new Point(-7, 2)),   //A
                new Triangle(new Point(-3, 0), new Point(0, 3), new Point(0, -3)),  //I
                new Triangle(new Point(3, -2), new Point(-2, -3), new Point(3, 3)), //A
                new Triangle(new Point(0, 0), new Point(3, 0), new Point(1.5, 2)),  //I
                new Triangle(new Point(1, 3), new Point(2, -5), new Point(-8, 4)),  //A
                new Triangle(new Point(0, 0), new Point(3, 0), new Point(1.5, 2.598076)),    //E
                new Triangle(new Point(0, 2), new Point(2, 6), new Point(4, 2)),    //I
                new Triangle(new Point(0, 0), new Point(4, 0), new Point(0, -3)),   //R
                new Triangle(new Point(9, 3), new Point(2, 10), new Point(2, 3)),   //R,I
                new Triangle(new Point(0, 0), new Point(6, 0), new Point(3, 5.196152))    //I
        };
        TriangleProcessor triangleProcessor = new TriangleProcessor(triangles);
        System.out.println();
        System.out.println("There are " + triangleProcessor.getCount(Type.EQUILATERAL) +
                " Equilateral triangles in the array.");
        System.out.println("There are " + triangleProcessor.getCount(Type.RECTANGULAR) +
                " Rectangular triangles in the array.");
        System.out.println("There are " + triangleProcessor.getCount(Type.ISOSCELES) +
                " Isosceles triangles in the array.");
        System.out.println("There are " + triangleProcessor.getCount(Type.ARBITRARY) +
                " Arbitrary triangles in the array.");
        System.out.println();



        System.out.println("The min square of Equilateral triangles is " +
                triangleProcessor.findMinEquilateralBySquare(triangles).getSquare() + "cm\u00B2. " +
                "and it has vertex such as: " + triangleProcessor.findMinEquilateralBySquare(triangles));
        System.out.println("The min square of Rectangular triangles is " +
                triangleProcessor.findMinRectangularBySquare(triangles).getSquare() + "cm\u00B2. " +
                "and it has vertex such as: " + triangleProcessor.findMinRectangularBySquare(triangles));
        System.out.println("The min square of Isosceles triangles is " +
                triangleProcessor.findMinIsoscelesBySquare(triangles).getSquare() + "cm\u00B2. " +
                "and it has vertex such as: " + triangleProcessor.findMinIsoscelesBySquare(triangles));
        System.out.println("The min square of Arbitrary triangles is " +
                triangleProcessor.findMinArbitraryBySquare(triangles).getSquare() + "cm\u00B2. " +
                "and it has vertex such as: " + triangleProcessor.findMinArbitraryBySquare(triangles));

        System.out.println();
        System.out.println("The max square of Equilateral triangles is " +
                triangleProcessor.findMaxEquilateralBySquare(triangles).getSquare() + "cm\u00B2. " +
                "and it has vertex such as: " + triangleProcessor.findMaxEquilateralBySquare(triangles));
        System.out.println("The max square of Rectangular triangles is " +
                triangleProcessor.findMaxRectangularBySquare(triangles).getSquare() + "cm\u00B2. " +
                "and it has vertex such as: " + triangleProcessor.findMaxRectangularBySquare(triangles));
        System.out.println("The max square of Isosceles triangles is " +
                triangleProcessor.findMaxIsoscelesBySquare(triangles).getSquare() + "cm\u00B2. " +
                "and it has vertex such as: " + triangleProcessor.findMaxIsoscelesBySquare(triangles));
        System.out.println("The max square of Arbitrary triangles is " +
                triangleProcessor.findMaxArbitraryBySquare(triangles).getSquare() + "cm\u00B2. " +
                "and it has vertex such as: " + triangleProcessor.findMaxArbitraryBySquare(triangles));

        System.out.println();
        System.out.println("The min perimeter of Equilateral triangles is " +
                triangleProcessor.findMinEquilateralByPerimeter(triangles).getPerimeter() + "cm. " +
                "and it has vertex such as: " + triangleProcessor.findMinEquilateralByPerimeter(triangles));
        System.out.println("The min perimeter of Rectangular triangles is " +
                triangleProcessor.findMinRectangularByPerimeter(triangles).getPerimeter() + "cm. " +
                "and it has vertex such as: " + triangleProcessor.findMinRectangularByPerimeter(triangles));
        System.out.println("The min perimeter of Isosceles triangles is " +
                triangleProcessor.findMinIsoscelesByPerimeter(triangles).getPerimeter() + "cm. " +
                "and it has vertex such as: " + triangleProcessor.findMinIsoscelesByPerimeter(triangles));
        System.out.println("The min perimeter of Arbitrary triangles is " +
                triangleProcessor.findMinArbitraryByPerimeter(triangles).getPerimeter() + "cm. " +
                "and it has vertex such as: " + triangleProcessor.findMinArbitraryByPerimeter(triangles));

                System.out.println();
        System.out.println("The max perimeter of Equilateral triangles is " +
                triangleProcessor.findMaxEquilateralByPerimeter(triangles).getPerimeter() + "cm. " +
                "and it has vertex such as: " + triangleProcessor.findMaxEquilateralByPerimeter(triangles));
        System.out.println("The max perimeter of Rectangular triangles is " +
                triangleProcessor.findMaxRectangularByPerimeter(triangles).getPerimeter() + "cm. " +
                "and it has vertex such as: " + triangleProcessor.findMaxRectangularByPerimeter(triangles));
        System.out.println("The max perimeter of Isosceles triangles is " +
                triangleProcessor.findMaxIsoscelesByPerimeter(triangles).getPerimeter() + "cm. " +
                "and it has vertex such as: " + triangleProcessor.findMaxIsoscelesByPerimeter(triangles));
        System.out.println("The max perimeter of Arbitrary triangles is " +
                triangleProcessor.findMaxArbitraryByPerimeter(triangles).getPerimeter() + "cm. " +
                "and it has vertex such as: " + triangleProcessor.findMaxArbitraryByPerimeter(triangles));
    }
}
