package com.hillel.archive.lesson11_enum.lection;

public class Example {

    enum Apple {
        JONATHAN,
        GOLDENDEL,
        RADDEL,
        WINESAP,
        CORTLAND;
    }


    public static void main(String[] args) {
        Apple theApple;

        theApple = Apple.JONATHAN;

        System.out.println("Value is : "  + theApple);
        System.out.println();

        theApple = Apple.GOLDENDEL;
        if (theApple == Apple.GOLDENDEL) {
            System.out.println("'theApple' contains GOLDENDEL");
            System.out.println("Name is " + Apple.JONATHAN.name());
            System.out.println("HashCode is " + Apple.JONATHAN.hashCode());
            System.out.println("HashCode is " + Apple.GOLDENDEL.hashCode());
            System.out.println("toString " + Apple.JONATHAN.toString());

        }

        for (Apple apple : Apple.values()) {
            System.out.println(apple);
        }
        System.out.println();

        theApple = Apple.valueOf("CORTLAND");
//        theApple = Apple.valueOf("Cortland");

        switch (theApple) {
            case JONATHAN:
                System.out.println("Jonathan is red");
                break;
            case RADDEL:
                System.out.println("RadDel is red");
                break;
            case WINESAP:
                System.out.println("Winesap is red");
                break;
            case CORTLAND:
                System.out.println("Cortland is red");
                break;
            case GOLDENDEL:
                System.out.println("GoldenDel is yellow");
                break;
        }
    }
}
