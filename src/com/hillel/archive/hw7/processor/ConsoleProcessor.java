package com.hillel.archive.hw7.processor;

import java.util.Scanner;

import com.hillel.archive.hw7.exception.NegativeValueException;

public class ConsoleProcessor {
    private Florist florist;

    public ConsoleProcessor(Florist florist) {
        this.florist = florist;
        showMainMenu();
    }

    private void showMainMenu() {
        Scanner scanner = new Scanner(System.in);
        boolean exit = false;
        while (!(exit)) {
            System.out.println();
            System.out.println("1: Create a bouquet");
            System.out.println("2: Operations on a bouquet");
            System.out.println("3: Print current bouquet");
            System.out.println("4: Print all of bouquets");
            System.out.println("0: Exit");
            while (!scanner.hasNextInt()) {
                System.out.println("Digits, please!");
                scanner.nextLine();
            }
            int val = scanner.nextInt();
            switch (val) {
                case 1:
                    showCreateBouquetMenu();
                    break;
                case 2:
                    showBouquetsMenu();
                    break;
                case 3:
                    printCurrentBouquet();
                    break;
                case 4:
                    printAllOfBouquets();
                    break;
                case 0:
                    exit = true;
                    break;
            }
        }
    }

    private void showCreateBouquetMenu() {
        Scanner scanner = new Scanner(System.in);
        florist.createBouquet();
        boolean exit = false;
        while (!(exit)) {
            System.out.println("1: Create flowers");
            System.out.println("2: Create accessories");
            System.out.println("0: Back");
            while (!scanner.hasNextInt()) {
                System.out.println("Digits, please!");
                scanner.nextLine();
            }
            int val = scanner.nextInt();
            switch (val) {
                case 1:
                    createFlowersMenu();
                    break;
                case 2:
                    createAccessoriesMenu();
                    break;
                case 0:
                    if (florist.appendBouquet()) {
                        System.out.print("Created bouquet");
                        showBouquetCost();
                    } else {
                        System.out.println("No one bouquets was created");
                    }
                    exit = true;
                    break;
            }
        }
    }

    private void showBouquetsMenu() {
        Scanner scanner = new Scanner(System.in);
        boolean exit = false;
        while (!(exit)) {
            System.out.println("Choose a bouquet by its index: ");
            printAllOfBouquets();
            System.out.println("0: Back");
            while (!scanner.hasNextInt()) {
                System.out.println("Digits, please!");
                scanner.nextLine();
            }
            int val = scanner.nextInt();
            switch (val) {
                case 0:
                    exit = true;
                    break;
                default:
                    florist.setCurrentBouquet(florist.getBouquets()[val - 1]);
                    showOperationsOnBouquetMenu();
            }
        }
    }

    private void printCurrentBouquet() {
        System.out.print((florist.getCurrentBouquet() != null) ? florist.getCurrentBouquet() : "There isn't any bouquets in store");
    }

    private void printAllOfBouquets() {
        System.out.print(florist.toString(florist.getBouquets()));
    }

    private void createFlowersMenu() {
        Scanner scanner = new Scanner(System.in);
        boolean exit = false;
        while (!(exit)) {
            System.out.println("1: New flower");
            System.out.println("0: Back");
            while (!scanner.hasNextInt()) {
                System.out.println("Digits, please!");
                scanner.nextLine();
            }
            int val = scanner.nextInt();
            switch (val) {
                case 1:
                    newFlower();
                    break;
                case 0:
                    exit = true;
                    break;
            }
        }
    }

    private void newFlower() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Name of the flower: ");
        String name = scanner.nextLine();
        try {
            System.out.print("Cost of the flower: ");
            double cost = scanner.nextDouble();
            System.out.print("Days of live of the flower: ");
            int daysOfLive = scanner.nextInt();
            System.out.print("Stem length of the flower: ");
            double stemLength = scanner.nextDouble();
            System.out.println();
            florist.appendUnit(name, stemLength, daysOfLive, cost);
        } catch (NegativeValueException e) {
            System.out.println(e);
        }
    }

    private void createAccessoriesMenu() {
        Scanner scanner = new Scanner(System.in);
        boolean exit = false;
        while (!(exit)) {
            System.out.println("1: New accessory");
            System.out.println("0: Back");
            while (!scanner.hasNextInt()) {
                System.out.println("Digits, please!");
                scanner.nextLine();
            }
            int val = scanner.nextInt();
            switch (val) {
                case 1:
                    newAccessory();
                    break;
                case 0:
                    exit = true;
                    break;
            }
        }
    }

    private void newAccessory() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Name of the accessory: ");
        String name = scanner.nextLine();
        try {
            System.out.print("Cost of the accessory: ");
            double cost = scanner.nextDouble();
            florist.appendUnit(name, cost);
        } catch (NegativeValueException e) {
            System.out.println(e);
        }
    }

    private void showOperationsOnBouquetMenu() {
        Scanner scanner = new Scanner(System.in);
        boolean exit = false;
        while (!(exit)) {
            printCurrentBouquet();
            System.out.println("1: Sort flowers by days to live");
            System.out.println("2: Find flowers by stem length");
            System.out.println("3: Bouquet cost");
            System.out.println("4: Add flower");
            System.out.println("5: Add accessory");
            System.out.println("0: Back");
            while (!scanner.hasNextInt()) {
                System.out.println("Digits, please!");
                scanner.nextLine();
            }
            int val = scanner.nextInt();
            switch (val) {
                case 1:
                    showSortFlowersByDaysOfLiveMenu();
                    break;
                case 2:
                    showFindFlowersByStemLengthMenu();
                    break;
                case 3:
                    showBouquetCost();
                    break;
                case 4:
                    newFlower();
                    florist.getCurrentBouquet().sortUnitsByType();
                    break;
                case 5:
                    newAccessory();
                    break;
                case 0:
                    exit = true;
                    printCurrentBouquet();
                    showMainMenu();
                    break;
            }
        }
    }

    private void showSortFlowersByDaysOfLiveMenu() {
        florist.sortFlowersByDaysOfLive();
    }

    private void showFindFlowersByStemLengthMenu() {
        Scanner scanner = new Scanner(System.in);
        scanner.nextLine();
        System.out.print("Min stem length: ");
        int minStemLength = scanner.nextInt();
        System.out.print("Max stem length: ");
        int maxStemLength = scanner.nextInt();
        System.out.println();
        System.out.println(florist.toString(florist.findFlowersByStemLength(minStemLength, maxStemLength)));
    }

    private void showBouquetCost() {
        System.out.println("costs " + florist.getCurrentBouquet().getCost() + "UAH");
    }
}
