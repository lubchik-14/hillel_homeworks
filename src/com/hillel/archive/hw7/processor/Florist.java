package com.hillel.archive.hw7.processor;

import com.hillel.archive.hw7.exception.NegativeValueException;
import com.hillel.archive.hw7.bouquets.FlowerBouquet;
import com.hillel.archive.hw7.units.Flower;
import com.hillel.archive.hw7.units.UnitOfBouquet;

public class Florist {
    private FlowerBouquet[] bouquets = {};
    private FlowerBouquet currentBouquet;

    public void createBouquet() {
        currentBouquet = new FlowerBouquet();
    }

    public boolean appendBouquet() {
        if (currentBouquet.getUnits().length != 0) {
            currentBouquet.sortUnitsByType();
            FlowerBouquet[] biggerBouquets = new FlowerBouquet[bouquets.length + 1];
            for (int i = 0; i < bouquets.length; i++) {
                biggerBouquets[i] = bouquets[i];
            }
            biggerBouquets[biggerBouquets.length - 1] = currentBouquet;
            bouquets = biggerBouquets;
            return true;
        }
        return false;
    }

    public void sortFlowersByDaysOfLive() {
        UnitOfBouquet buff;
        for (int j = 1; j < currentBouquet.getUnits().length - 1; j++) {
            int countExchanges = 0;
            for (int i = 0; i < currentBouquet.getFlowers().length - j; i++) {
                if ((((Flower) currentBouquet.getUnits()[i]).getDaysOfLive())
                        > (((Flower) currentBouquet.getUnits()[i + 1]).getDaysOfLive())) {
                    countExchanges = 1;
                    buff = currentBouquet.getUnits()[i];
                    currentBouquet.getUnits()[i] = currentBouquet.getUnits()[i + 1];
                    currentBouquet.getUnits()[i + 1] = buff;
                }
            }
            if (countExchanges == 0) {
                break;
            }
        }
    }

    public UnitOfBouquet[] findFlowersByStemLength(int minStemLength, int maxStemLength) {
        UnitOfBouquet[] foundUnits = {};
        UnitOfBouquet[] flowers = currentBouquet.getFlowers();
        for (UnitOfBouquet unit : flowers) {
            if ((((Flower) unit).getLengthOfStem() >= minStemLength)
                    && (((Flower) unit).getLengthOfStem() <= maxStemLength)) {
                UnitOfBouquet[] biggerUnitsOfBouquet = new UnitOfBouquet[foundUnits.length + 1];
                for (int i = 0; i < foundUnits.length; i++) {
                    biggerUnitsOfBouquet[i] = foundUnits[i];
                }
                biggerUnitsOfBouquet[biggerUnitsOfBouquet.length - 1] = unit;
                foundUnits = biggerUnitsOfBouquet;
            }
        }
        return foundUnits;
    }

    public void appendUnit(String name, double stemLength, int daysOfLive, double cost) throws NegativeValueException {
        if (stemLength <= 0) {
            throw new NegativeValueException("Stem length");
        } else if (daysOfLive <= 0) {
            throw new NegativeValueException("Days of live");
        } else if (cost <= 0) {
            throw new NegativeValueException("Cost");
        } else {
            currentBouquet.appendUnit(new Flower(name, stemLength, daysOfLive, cost));
        }
    }

    public void appendUnit(String name, double cost) throws NegativeValueException {
        if (cost <= 0) {
            throw new NegativeValueException("Cost");
        } else {
            currentBouquet.appendUnit(new UnitOfBouquet(name, cost));
        }
    }

    public String toString(FlowerBouquet[] bouquets) {
        String print = "";
        for (int i = 0; i < bouquets.length; i++) {
            print += (i + 1) + ": " + bouquets[i] + "\n";
        }
        return print + "\b";
    }

    public String toString(UnitOfBouquet[] unitsOfBouquet) {
        System.out.println("Found: ");
        String print = "";
        for (UnitOfBouquet unitOfBouquet : unitsOfBouquet) {
            print += unitOfBouquet + "\n";
        }
        return print + "\b";
    }

    public FlowerBouquet[] getBouquets() {
        return bouquets;
    }

    public void setBouquets(FlowerBouquet[] bouquets) {
        this.bouquets = bouquets;
    }

    public FlowerBouquet getCurrentBouquet() {
        return currentBouquet;
    }

    public void setCurrentBouquet(FlowerBouquet currentBouquet) {
        this.currentBouquet = currentBouquet;
    }
}
