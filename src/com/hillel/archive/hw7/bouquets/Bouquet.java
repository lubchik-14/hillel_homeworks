package com.hillel.archive.hw7.bouquets;

import com.hillel.archive.hw7.units.UnitOfBouquet;

public interface Bouquet {
    void appendUnit(UnitOfBouquet unitOfBouquet);

    void sortUnitsByType();

    double getCost();

    UnitOfBouquet[] getFlowers();

    UnitOfBouquet[] getAccessories();

}
