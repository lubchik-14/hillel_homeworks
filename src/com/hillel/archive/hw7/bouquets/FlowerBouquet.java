package com.hillel.archive.hw7.bouquets;

import com.hillel.archive.hw7.units.Flower;
import com.hillel.archive.hw7.units.UnitOfBouquet;


public class FlowerBouquet implements Bouquet {
    private UnitOfBouquet[] unitsOfBouquet = {};

    public FlowerBouquet() {
        this.unitsOfBouquet = new UnitOfBouquet[0];
    }

    public FlowerBouquet(UnitOfBouquet[] unitsOfBouquet) {
        this.unitsOfBouquet = unitsOfBouquet;
    }

    public UnitOfBouquet[] appendUnit(UnitOfBouquet[] unitsOfBouquet, UnitOfBouquet unitOfBouquet) {
        UnitOfBouquet[] biggerUnitsOfBouquet = new UnitOfBouquet[unitsOfBouquet.length + 1];
        for (int i = 0; i < unitsOfBouquet.length; i++) {
            biggerUnitsOfBouquet[i] = unitsOfBouquet[i];
        }
        biggerUnitsOfBouquet[biggerUnitsOfBouquet.length - 1] = unitOfBouquet;
        return biggerUnitsOfBouquet;
    }

    public void appendUnit(UnitOfBouquet unitOfBouquet) {
        unitsOfBouquet = appendUnit(unitsOfBouquet, unitOfBouquet);
    }

    public double getCost() {
        double cost = 0;
        for (UnitOfBouquet unitOfBouquet : unitsOfBouquet) {
            cost += unitOfBouquet.getCost();
        }
        return cost;
    }

    public void sortUnitsByType() {
        UnitOfBouquet[] accessories = getAccessories();
        UnitOfBouquet[] flowers = getFlowers();
        if (flowers.length == unitsOfBouquet.length) {
            for (int i = 0; i < flowers.length; i++) {
                unitsOfBouquet[i] = flowers[i];
            }
        } else if (accessories.length == unitsOfBouquet.length) {
            for (int i = 0; i < accessories.length; i++) {
                unitsOfBouquet[i] = accessories[i];
            }
        } else {
            for (int i = 0; i < flowers.length; i++) {
                unitsOfBouquet[i] = flowers[i];
            }
            for (int i = flowers.length, j = 0; j < accessories.length; i++, j++) {
                unitsOfBouquet[i] = accessories[j];
            }
        }
    }

    public UnitOfBouquet[] getFlowers() {
        UnitOfBouquet[] flowers = {};
        for (UnitOfBouquet unitOfBouquet : unitsOfBouquet) {
            if (unitOfBouquet instanceof Flower) {
                flowers = appendUnit(flowers, unitOfBouquet);
            }
        }
        return flowers;
    }

    public UnitOfBouquet[] getAccessories() {
        UnitOfBouquet[] units = {};
        for (UnitOfBouquet unitOfBouquet : unitsOfBouquet) {
            if (!(unitOfBouquet instanceof Flower)) {
                units = appendUnit(units, unitOfBouquet);
            }
        }
        return units;
    }

    @Override
    public String toString() {
        String print = "";
        for (UnitOfBouquet unitOfBouquet : unitsOfBouquet) {
            print += unitOfBouquet + "\n";
        }
        return "Bouquet " + "\n" + print + "\b";
    }

    public UnitOfBouquet[] getUnits() {
        return unitsOfBouquet;
    }

    public void setUnits(UnitOfBouquet[] unitsOfBouquet) {
        this.unitsOfBouquet = unitsOfBouquet;
    }
}
