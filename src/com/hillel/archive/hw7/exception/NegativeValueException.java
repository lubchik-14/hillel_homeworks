package com.hillel.archive.hw7.exception;

public class NegativeValueException extends Exception {
    String arg;

    public NegativeValueException(String arg) {
        this.arg = arg;
    }

    @Override
    public String toString() {
        return "Only positive values! " + arg + " is wrong.";
    }

}
