package com.hillel.archive.hw7.units;

public class UnitOfBouquet {
    private double cost;
    private String name;

    public UnitOfBouquet(String name, double cost) {
        this.cost = cost;
        this.name = name;
    }

    @Override
    public String toString() {
        return "\t\t"
                + name + "\t"
                + "cost=" + cost + " UAH";
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getCost() {
        return cost;
    }

    public void setCost(double cost) {
        this.cost = cost;
    }
}
