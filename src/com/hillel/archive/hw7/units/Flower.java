package com.hillel.archive.hw7.units;

public class Flower extends UnitOfBouquet {
    private double lengthOfStem;
    private int daysOfLive;

    public Flower(String name, double lengthOfStem, int daysOfLive, double cost) {
        super(name, cost);
        this.lengthOfStem = lengthOfStem;
        this.daysOfLive = daysOfLive;
    }

    @Override
    public String toString() {
        return super.toString()
                + ", daysOfLive=" + daysOfLive
                + ", length=" + lengthOfStem;
    }

    public double getLengthOfStem() {
        return lengthOfStem;
    }

    public void setLengthOfStem(double lengthOfStem) {
        this.lengthOfStem = lengthOfStem;
    }

    public int getDaysOfLive() {
        return daysOfLive;
    }

    public void setDaysOfLive(int daysOfLive) {
        this.daysOfLive = daysOfLive;
    }
}
