package com.hillel.archive.hw5.HW5.src;

public class Refrigerator extends Truck {
    int quantityOfChambers;
    double minTemperature;
    double maxTemperature;

    Refrigerator(int id, String brand, String model, double fuelEconomy, int maxSpeed, double cost,
                 boolean canUseTrailer, double maxWeightLimit, int quantityOfChambers, double minTemperature,
                 double maxTemperature) {
        super(id, brand, model, fuelEconomy, maxSpeed, cost, canUseTrailer, maxWeightLimit);
        this.quantityOfChambers = quantityOfChambers;
        this.minTemperature = minTemperature;
        this.maxTemperature = maxTemperature;
    }

    public String toString() {
        return "ID: " + id + " Refrigerator\t\t" + brand + " " + model + ",\t" + fuelEconomy + " km/L,\t" + maxSpeed +
                " km an hour,\t" + " using a trailer: " + canUseTrailer + ",\tlimit of weight " + maxWeightLimit +
                ",\t" +  quantityOfChambers + " chambers,\t" + minTemperature + "/+" + maxTemperature + " C\u00b0" +
                "\t($ " + cost + ")";
    }
}