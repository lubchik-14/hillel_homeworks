package com.hillel.archive.hw5.HW5.src;

abstract class Bus extends Vehicle {
    int passengerCapacity;

    Bus(int id, String brand, String model, double fuelEconomy, int maxSpeed, double cost, int passengerCapacity) {
        super(id, brand, model, fuelEconomy, maxSpeed, cost);
        this.passengerCapacity = passengerCapacity;
    }
}
