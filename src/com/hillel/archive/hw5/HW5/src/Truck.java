package com.hillel.archive.hw5.HW5.src;

abstract class Truck extends Vehicle {
    boolean canUseTrailer;
    double maxWeightLimit;

    Truck(int id, String brand, String model, double fuelEconomy, int maxSpeed, double cost, boolean canUseTrailer,
          double maxWeightLimit) {
        super(id, brand, model, fuelEconomy, maxSpeed, cost);
        this.canUseTrailer = canUseTrailer;
        this.maxWeightLimit = maxWeightLimit;
    }
}
