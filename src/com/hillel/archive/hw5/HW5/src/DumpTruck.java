package com.hillel.archive.hw5.HW5.src;

public class DumpTruck extends Truck {
    double maxVolumeOfSmth;
    boolean isLeftOrRightGate;

    DumpTruck(int id, String brand, String model, double fuelEconomy, int maxSpeed, double cost,
              boolean canUseTrailer, double maxWeightLimit, double maxVolumeOfSmth, boolean isLeftOrRightGate) {
        super(id, brand, model, fuelEconomy, maxSpeed, cost, canUseTrailer, maxWeightLimit);
        this.maxVolumeOfSmth = maxVolumeOfSmth;
        this.isLeftOrRightGate = isLeftOrRightGate;
    }

    public String toString() {
        return "ID: " + id + " Dump Truck\t\t" + brand + " " + model + ",\t" + fuelEconomy + " km/L,\t" + maxSpeed +
                " km an hour,\t" + " using a trailer: " + canUseTrailer + "\tlimit of weight " + maxWeightLimit +
                ",\tlimit of volume " + maxVolumeOfSmth + ",\tright/left opening: " + isLeftOrRightGate + "\t($ " + cost + ")";
    }
}