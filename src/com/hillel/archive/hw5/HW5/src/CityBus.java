package com.hillel.archive.hw5.HW5.src;

public class CityBus extends Bus {
    int quantityOfDisabled;
    int quantityOfCeilingMountedHandles;

    CityBus (int id, String brand, String model, double fuelEconomy, int maxSpeed, double cost, int totalNumberOfSeats,
             int quantityOfDisabled, int quantityOfCeilingMountedHandles) {
        super(id, brand, model, fuelEconomy, maxSpeed, cost, totalNumberOfSeats);
        this.quantityOfDisabled = quantityOfDisabled;
        this.quantityOfCeilingMountedHandles = quantityOfCeilingMountedHandles;
    }

    public String toString() {
        return "ID: " + id + " City Bus\t\t\t" + brand + " " + model + ",\t" + fuelEconomy + " km/L,\t" + maxSpeed +
                " km an hour,\t" + passengerCapacity + " seats,\t" + quantityOfDisabled +
                " rooms for disabled people, \t" + quantityOfCeilingMountedHandles + " ceiling mounted handles ($ " +
                cost + ")";

    }
}