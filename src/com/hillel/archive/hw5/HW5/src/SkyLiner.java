package com.hillel.archive.hw5.HW5.src;

public class SkyLiner extends Bus {
    int quantityOfTvs;
    int quantityOfFloors;

    SkyLiner (int id, String brand, String model, double fuelEconomy, int maxSpeed, double cost, int totalNumberOfSeats,
             int quantityOfTvs, int quantityOfFloors) {
        super(id, brand, model, fuelEconomy, maxSpeed, cost, totalNumberOfSeats);
        this.quantityOfTvs = quantityOfTvs;
        this.quantityOfFloors = quantityOfFloors;
    }

    public String toString() {
        return "ID: " + id + " SkyLiner\t\t\t" + brand + " " + model + ",\t" + fuelEconomy + " km/L,\t" + maxSpeed +
                " km an hour,\t" + passengerCapacity + " seats,\t" + quantityOfFloors +
                " floors,\t" + quantityOfTvs + " TVs ($ " + cost + ")";

    }
}
