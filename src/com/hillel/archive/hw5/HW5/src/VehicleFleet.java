package com.hillel.archive.hw5.HW5.src;

public class VehicleFleet {
    private Vehicle[] vehicles;

    VehicleFleet(Vehicle[] vehicles) {
        this.vehicles = vehicles;
    };

    double countCosts() {
        double sum = 0;
        for (Vehicle i : vehicles) {
        sum += i.cost;
        }
        return sum;
    }

    void sortByFuelEconomy() {
        Vehicle buff;
        for (int j = 1; j < vehicles.length - 1; j++) {
            int f = 0;
            for (int i = 0; i < vehicles.length - j; i++) {
                if (vehicles[i].fuelEconomy > vehicles[i + 1].fuelEconomy) {
                    f = 1;
                    buff = vehicles[i];
                    vehicles[i] = vehicles[i + 1];
                    vehicles[i + 1] = buff;
                }
            }
            if (f == 0) {
                break;
            }
        }
    }

    Vehicle[] findBySpeedRange(int minSpeed, int maxSpeed) {
        Vehicle[] found = new Vehicle[0];
        Vehicle[] foundG = new Vehicle[0];
        for (Vehicle vehicle : vehicles) {
            if ((vehicle.maxSpeed >= minSpeed) && (vehicle.maxSpeed <= maxSpeed)) {
                foundG = new Vehicle[foundG.length + 1];
                for (int i = 0; i < found.length; i++) {
                    foundG[i] = found[i];
                }
                foundG[foundG.length - 1] = vehicle;
                found = foundG;
            }
        }
        return foundG;
    }
}
