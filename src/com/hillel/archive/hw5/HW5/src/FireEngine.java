package com.hillel.archive.hw5.HW5.src;

public class FireEngine extends Truck {
    double limitOfWaterVolume;
    int quantityOfLights;
    int quantityOfLadders;
    FireEngine(int id, String brand, String model, double fuelEconomy, int maxSpeed, double cost,
               boolean canUseTrailer, double maxWeightLimit, double limitOfWaterVolume, int quantityOfLights,
               int quantityOfLadders) {
        super(id, brand, model, fuelEconomy, maxSpeed, cost, canUseTrailer, maxWeightLimit);
        this.limitOfWaterVolume = limitOfWaterVolume;
        this.quantityOfLights = quantityOfLights;
        this.quantityOfLadders = quantityOfLadders;
    }

    public String toString() {
        return "ID: " + id + " Fire Engine\t\t" + brand + " " + model + ",\t" + fuelEconomy + " km/L,\t" + maxSpeed +
                " km an hour,\t" + " using a trailer: " + canUseTrailer + ",\tlimit of weight " + maxWeightLimit +
                ",\tlimit of volume " + limitOfWaterVolume + ",\t" + quantityOfLights + " lights, \t" +
                quantityOfLadders + " ladders" + "\t($ " + cost + ")";
    }
}