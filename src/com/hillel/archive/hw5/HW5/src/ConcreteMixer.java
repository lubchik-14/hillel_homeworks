package com.hillel.archive.hw5.HW5.src;

public class ConcreteMixer extends Truck {
    double maxVolumeOfMixture;

    ConcreteMixer(int id, String brand, String model, double fuelEconomy, int maxSpeed, double cost,
                  boolean canUseTrailer, double maxWeightLimit, double maxVolumeOfMixture) {
        super(id, brand, model, fuelEconomy, maxSpeed, cost, canUseTrailer, maxWeightLimit);
        this.maxVolumeOfMixture = maxVolumeOfMixture;
    }

    public String toString() {
        return "ID: " + id + " ConcreteMixer\t\t" + brand + " " + model + ",\t" + fuelEconomy + " km/L,\t" + maxSpeed +
                " km an hour,\t" + " using a trailer: " + canUseTrailer + "\tlimit of weight " + maxWeightLimit +
                ",\tlimit of volume " + maxVolumeOfMixture + "\t($ " + cost + ")";
    }
}
