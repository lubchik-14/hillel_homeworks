package com.hillel.archive.hw5.HW5.src;

public class Vehicle {
    int id;
    int maxSpeed;
    double cost;
    double fuelEconomy;
    String brand;
    String model;

    Vehicle(int id, String brand, String model, double fuelEconomy, int maxSpeed, double cost) {
        this.id = id;
        this.brand = brand;
        this.model = model;
        this.maxSpeed = maxSpeed;
        this.cost = cost;
        this.fuelEconomy = fuelEconomy;
    }
}
