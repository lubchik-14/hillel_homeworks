package com.hillel.archive.hw5.HW5.src;

public class SchoolBus extends Bus {
    int quantityOfSeatBelts;
    int quantityOfEmergencyExits;

    SchoolBus (int id, String brand, String model, double fuelEconomy, int maxSpeed, double cost, int totalNumberOfSeats,
                          int quantityOfSeatBelts, int quantityOfEmergencyExits) {
        super(id, brand, model, fuelEconomy, maxSpeed, cost, totalNumberOfSeats);
        this.quantityOfSeatBelts = quantityOfSeatBelts;
        this.quantityOfEmergencyExits = quantityOfEmergencyExits;
    }

    public String toString() {
        return "ID: " + id + " School Bus\t\t" + brand + " " + model + ",\t" + fuelEconomy + " km/L,\t" + maxSpeed +
                " km an hour,\t" + passengerCapacity + " seats,\t" + quantityOfSeatBelts +
                " seat belts,\t" + quantityOfEmergencyExits + " emergency exits ($ " + cost + ")";

    }
}
