package com.hillel.archive.hw5.HW5.src;

public class Runner {

    Runner() {
        Vehicle[] vehicles = new Vehicle[]{
                new DumpTruck(1, "GINAF", "G 5450", 100, 70, 50000,
                        true, 31000, 24000, true),
                new CityBus(2, "Богдан", "А601.10", 24, 80, 115375.99,
                        27, 0, 10),
                new SkyLiner(3, "Neoplan", "N316 SHD", 12.4, 128, 133864,
                        52, 4, 2),
                new ConcreteMixer(4, "Renault", "Kerax 385", 18, 90, 35000,
                        false, 3500, 10000),
                new CityBus(5, "ПАЗ", "32054", 15, 95, 48999.99,
                        20, 1, 0),
                new DumpTruck(6, "БЕЛАЗ", "75710", 1300, 67, 10000000,
                        false, 450000, 15000, false),
                new FireEngine(7, "ГАЗ", "2705 АПП-4-276", 60, 170, 40000,
                        true, 2000, 2500, 30,
                        1),
                new Refrigerator(8, "Renault", "Premium", 27.5, 90, 25000,
                        true, 2100, 1, -20, +20),
                new SkyLiner(9, "Mercedes-Benz", "Tourismo L", 15, 150, 150000,
                        55, 10, 1),
                new SchoolBus(10, "IC", "Nexbus XL", 8.8, 100, 175000,
                        57, 56, 3),
                new ConcreteMixer(11, "КамАЗ", "53229", 20, 85, 20000,
                        false, 2500, 7000),
                new SchoolBus(12, "Thomas Built Buses", "Freightliner C2", 16, 130,
                        215000, 50, 50, 2),
                new Refrigerator(13, "Ford", "Transit", 30, 100, 20000,
                        true, 2000, 4, -30, +22),
                new FireEngine(14, "ЗИЛ", "АНР-40-1400", 50, 150, 59999.99,
                        false, 2500, 20000, 25,
                        3)
        };

        VehicleFleet vehicleFleet = new VehicleFleet(vehicles);
        System.out.println("VehicleFleet costs $" + vehicleFleet.countCosts());
        System.out.println();
        System.out.println("Unsorted VehicleFleet:");
        printVehicles(vehicles);
        System.out.println();
        vehicleFleet.sortByFuelEconomy();
        System.out.println("Sorted VehicleFleet:");
        printVehicles(vehicles);
        System.out.println();
        System.out.println("Vehicles that have maxSpeed in the range 75 to 200 km an hour:");
        printVehicles(vehicleFleet.findBySpeedRange(100, 300));
    }

    void printVehicles(Vehicle[] vehicles) {
        for (Vehicle vehicle : vehicles) {
            System.out.println(vehicle);
        }
    }
}
