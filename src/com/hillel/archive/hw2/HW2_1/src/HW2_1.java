package com.hillel.archive.hw2.HW2_1.src;

import java.util.Arrays;

public class HW2_1 {
    public static void main(String[] args) {
//        #1
        System.out.println("---------------  1  -------------");
        System.out.print("Elements of 'arr1' are: ");
        int[] arr = {7362, 128974, 56, 17, 81, 25, 39, 9999998, 159, 7425, 13, 5793, 246};
        for (int i : arr) {
            System.out.print(i + ", ");
        }
        System.out.println("\b\b");
        int[] evenArr = new int[0];
        int[] oddArr = evenArr;
        System.out.print("Even elements of 'arr' are: ");
        for (int i : arr) {
            if (i % 2 == 0) {
                System.out.print(i + ", ");
            }
        }
        System.out.println("\b\b");

        System.out.print("Odd elements of 'arr' are: ");
        for (int i : arr) {
            if (i % 2 != 0) {
                System.out.print(i + ", ");
            }
        }
        System.out.print("\b\b\n");
        for (int i : arr) {
            if (i % 2 == 0) {
                evenArr = Arrays.copyOf(evenArr, evenArr.length + 1);
                evenArr[evenArr.length - 1] = i;
            } else {
                oddArr = Arrays.copyOf(oddArr, oddArr.length + 1);
                oddArr[oddArr.length - 1] = i;
            }
        }
        int k = 0;
        int maxLength = evenArr.length > oddArr.length ? evenArr.length : oddArr.length;
        System.out.println("even" + "\t\t\t" + "odd");
        while (k < maxLength) {
            if (k < evenArr.length) {
                if ((evenArr[k] / 1000 < 1) && (k < evenArr.length)) {
                    System.out.println(evenArr[k] + "\t\t\t\t" + oddArr[k]);
                } else {
                    System.out.println(evenArr[k] + "\t\t\t" + oddArr[k]);
                }
            } else {
                System.out.println("\t\t\t\t" + oddArr[k]);
            }
            k++;
        }
    }
}
