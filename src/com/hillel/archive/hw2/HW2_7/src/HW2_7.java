package com.hillel.archive.hw2.HW2_7.src;

import java.util.Arrays;

public class HW2_7 {
    public static void main (String[] args) {
        //        #7
        System.out.println("---------------  7  -------------");
        int[] arr7 = {7362, 122398974, 656, 17, 881, 4, 25, 39, 999999998, 159, 7425, 13, 5793, 246};
        System.out.print("Elements of 'arr7' are: ");
        for (int i : arr7) {
            System.out.print(i + ", ");
        }
        System.out.println("\b\b ");
        System.out.print("The elements of 'arr7' consist of different digits are: ");
        for (int i = 0; i < arr7.length; i++) {
            int[] elementOfArr7 = new int[0];
            if ((arr7[i] > -11) && (arr7[i] < 11)) {
                System.out.print(arr7[i] + ", ");
            }
            int a = arr7[i];
            int count = 0;
            do {
                elementOfArr7 = Arrays.copyOf(elementOfArr7, elementOfArr7.length + 1);
                elementOfArr7[elementOfArr7.length - 1] = a%10;
                a /= 10;
            }
            while (a > 0);
            for (int j = 0; j < elementOfArr7.length; j++) {
                for (int l = j + 1; l < elementOfArr7.length; l++) {
                    if (elementOfArr7[j] == elementOfArr7[l]) {
                        count++;
                    }
                }
            }
            if (count == 0) {
                System.out.print(arr7[i] + ", ");
            }
        }
        System.out.println("\b\b ");
    }
}

