package com.hillel.archive.hw2.HW2_6.src;

public class HW2_6 {
    public static void main (String[] args) {
        //        #6
        System.out.println("---------------  6  -------------");

        int[] arr6 = {7362, 128974, 56, 17, 81, 4, 25, 39, 9999998, 159, 7425, 13, 5793, 246};
        System.out.print("Elements of 'arr6' are: ");
        for (int i : arr6) {
            System.out.print(i + ", ");
        }
        System.out.println("\b\b ");
        int[] countOfNum = new int[arr6.length];
        int min, max, a, count;
        max = min = 1;
        for (int i = 0; i < arr6.length; i++) {
            count = 1;
            a = arr6[i];
            if (a == 0) {
                count = 1;
            }
            else if (a < 0) {
                a = a * (-1);
            }
            while ((a / 10) > 0) {
                a /= 10;
                count++;
            }
            max = count > max ? count : max;
            min = count < min ? count : min;
            countOfNum[i] = count;
        }
        for (int i = 0; i < countOfNum.length; i++) {
            if (countOfNum[i] == max) {
                System.out.print("The max number of digits is: " + max + " (");
                System.out.println(arr6[i] + ")");
            }
            if (countOfNum[i] == min) {
                System.out.print("The min number of digits is: " + min + " (");
                System.out.println(arr6[i] + ")");
            }
        }
    }
}

