package com.hillel.archive.hw2.HW2_8.src;

import java.util.Scanner;

public class HW2_8 {
    public static void main (String[] args) {
        System.out.println("---------------  8  -------------");
        System.out.print("Enter number of values: ");
        Scanner num = new Scanner(System.in);
        int numOfValues = num.nextInt();
        for (int j = 1; j <= numOfValues; j++) {
            System.out.print(j + "\t");
            if (j%(int) (Math.sqrt(numOfValues) + 1) == 0 ) {
                System.out.print("\n");
            }
        }
    }
}

