package com.hillel.archive.hw2.HW2_2.src;

public class HW2_2 {
    public static void main (String[] args) {
        //        #2
        System.out.println("---------------  2  -------------");
        System.out.print("Elements of 'arr2' are: ");
        int[] arr2 = {1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144, 233, 377, 610, 987, 1597, 2584, 4181, 6765};
        for (int i : arr2) {
            System.out.print(i + ", ");
        }
        System.out.println("\b\b");
        System.out.print("Prime numbers of 'arr2' are: ");
        for (int n : arr2) {
            int count = 0;
            for (int i = 1; i <= n; i++) {
                if ((n%i == 0) && (n > 1)) {
                    count++;
                }
            }
            if (count == 2) {
                System.out.print(n + ", ");
            }
        }
        System.out.println("\b\b");
    }
}
