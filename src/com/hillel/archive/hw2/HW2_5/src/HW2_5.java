package com.hillel.archive.hw2.HW2_5.src;

import java.util.Scanner;

public class HW2_5 {
    public static void main (String[] args) {
        //        #5
        System.out.println("---------------  5  -------------");
        Scanner num = new Scanner(System.in);
        System.out.print("How many rows do matrices have?  ");
        int numOfRows = num.nextInt();
        System.out.print("How many columns do matrices have?  ");
        int numOfCols = num.nextInt();
        int[][] mtrxC = new int[numOfRows][numOfCols];
        int[][] mtrxB = new int[numOfRows][numOfCols];
        int[][] mtrxA = new int[numOfRows][numOfCols];
        int c = 0;
        for (int i = 0; i < mtrxA.length; i++) {
            for (int j = 0; j < mtrxA[i].length; j++) {
                mtrxA[i][j] = (-100) + (int) (Math.random() * 200);
                mtrxB[i][j] = (-100) + (int) (Math.random() * 200);
            }
        }
        for (int i = 0; i < mtrxA.length; i++) {
            for (int j = 0; j < mtrxB[i].length; j++) {
                for (int l = 0; l < mtrxA[i].length; l++) {
                    mtrxC[i][j] += mtrxA[i][l] * mtrxB[l][j];
                }
            }
        }
        System.out.println("The matrix A :");
        for (int i[] : mtrxA) {
            for (int j = 0; j < i.length; j++) {
                System.out.print(i[j] + "\t\t");
            }
            System.out.print("\n");
        }
        System.out.print("\n");
        System.out.println("The matrix B :");
        for (int i[] : mtrxB) {
            for (int j = 0; j < i.length; j++) {
                System.out.print(i[j] + "\t\t");
            }
            System.out.print("\n");
        }
        System.out.print("\n");
        System.out.println("The matrix C = A*B:");
        for (int i[] : mtrxC) {
            for (int j = 0; j < i.length; j++) {
                System.out.print(i[j] + "\t");
            }
            System.out.print("\n");
        }
    }
}

