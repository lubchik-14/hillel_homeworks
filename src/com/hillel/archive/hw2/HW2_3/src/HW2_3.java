package com.hillel.archive.hw2.HW2_3.src;

public class HW2_3 {
    public static void main (String[] args) {
        //        #3
        System.out.println("---------------  3  -------------");
        System.out.print("First twenty Fibonacci numbers are: 1, 1, ");
        int[] fibarr = new int[20];
        fibarr[0] = 1;
        fibarr[1] = 1;
        for (int i = 2; i < fibarr.length; i++) {
            fibarr[i] = fibarr[i - 2] + fibarr[i - 1];
            System.out.print(fibarr[i] + ", ");
        }
        System.out.println("\b\b");
    }
}
