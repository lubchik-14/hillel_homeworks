package com.hillel.archive.hw2.HW2_4.src;

public class HW2_4 {
    public static void main (String[] args) {
        //        #4
        System.out.println("---------------  4  -------------");
        double[] arr3 = {0.4, 0.9, -1.3, 8.55, 0, 6.79, -25.4, .1, .005, 5694.596, 0.0001, 5, 743.1};
        System.out.print("Elements of 'arr3' are: ");
        for (double i : arr3) {
            System.out.print(i + ", ");
        }
        double maxArr, minArr;
        maxArr = minArr = arr3[0];
        for (int i = 1; i < arr3.length; i++) {
            maxArr = arr3[i] > maxArr ? arr3[i] : maxArr;
            minArr = arr3[i] < minArr ? arr3[i] : minArr;
        }
        System.out.println("\nMin element is " + minArr);
        System.out.println("Max element is " + maxArr);
    }
}
